<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::prefix('import')->name('imports/')->group(static function() {
    Route::get('/organizations','ImportsController@importOrganizations')->name('organizations');
    Route::get('/owners','ImportsController@importOwners')->name('owners');
    Route::get('/routes','ImportsController@importRoutes')->name('routes');
    Route::get('/drivers','ImportsController@importDrivers')->name('drivers');
    Route::get('/conductors','ImportsController@importConductors')->name('conductors');
    Route::get('/inspectors','ImportsController@importInspectors')->name('inspectors');
    Route::get('/vehicles','ImportsController@importVehicles')->name('vehicles');
    Route::get('/transactions','ImportsController@importTransactions')->name('transactions');
    Route::get('/transactiondetails','ImportsController@importTransactionDetails')->name('transaction.details');


    Route::get('/parse','ImportsController@parseImport')->name('transaction.import');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('dashboard')->name('dashboard/')->group(static function() {
            Route::get('/','DashboardController@index')->name('index');
        });
    });
});


Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('reports')->name('reports/')->group(static function() {
            Route::get('/vehicles','ReportsController@index')->name('vehicles');
            Route::get('/owners','ReportsController@index')->name('owners');
            Route::get('/routes','ReportsController@index')->name('routes');
            Route::get('/drivers','ReportsController@index')->name('drivers');
            Route::get('/conductors','ReportsController@index')->name('conductors');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-users')->name('admin-users/')->group(static function() {
            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('organizations')->name('organizations/')->group(static function() {
            Route::get('/',                                             'OrganizationsController@index')->name('index');
            Route::get('/create',                                       'OrganizationsController@create')->name('create');
            Route::post('/',                                            'OrganizationsController@store')->name('store');
            Route::get('/{organization}/edit',                          'OrganizationsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'OrganizationsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{organization}',                              'OrganizationsController@update')->name('update');
            Route::delete('/{organization}',                            'OrganizationsController@destroy')->name('destroy');
            Route::get('/export',                                       'OrganizationsController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('organization-staffs')->name('organization-staffs/')->group(static function() {
            Route::get('/',                                             'OrganizationStaffController@index')->name('index');
            Route::get('/create',                                       'OrganizationStaffController@create')->name('create');
            Route::post('/',                                            'OrganizationStaffController@store')->name('store');
            Route::get('/{organizationStaff}/edit',                     'OrganizationStaffController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'OrganizationStaffController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{organizationStaff}',                         'OrganizationStaffController@update')->name('update');
            Route::delete('/{organizationStaff}',                       'OrganizationStaffController@destroy')->name('destroy');
            Route::get('/export',                                       'OrganizationStaffController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('owners')->name('owners/')->group(static function() {
            Route::get('/',                                             'OwnersController@index')->name('index');
            Route::get('/create',                                       'OwnersController@create')->name('create');
            Route::post('/',                                            'OwnersController@store')->name('store');
            Route::get('/{owner}/edit',                                 'OwnersController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'OwnersController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{owner}',                                     'OwnersController@update')->name('update');
            Route::delete('/{owner}',                                   'OwnersController@destroy')->name('destroy');
            Route::get('/export',                                       'OwnersController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('routes')->name('routes/')->group(static function() {
            Route::get('/',                                             'RoutesController@index')->name('index');
            Route::get('/create',                                       'RoutesController@create')->name('create');
            Route::post('/',                                            'RoutesController@store')->name('store');
            Route::get('/{route}/edit',                                 'RoutesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'RoutesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{route}',                                     'RoutesController@update')->name('update');
            Route::delete('/{route}',                                   'RoutesController@destroy')->name('destroy');
            Route::get('/export',                                       'RoutesController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('crew-staffs')->name('crew-staffs/')->group(static function() {
            Route::get('/',                                             'CrewStaffController@index')->name('index');
            Route::get('/create',                                       'CrewStaffController@create')->name('create');
            Route::post('/',                                            'CrewStaffController@store')->name('store');
            Route::get('/{crewStaff}/edit',                             'CrewStaffController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CrewStaffController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{crewStaff}',                                 'CrewStaffController@update')->name('update');
            Route::delete('/{crewStaff}',                               'CrewStaffController@destroy')->name('destroy');
            Route::get('/export',                                       'CrewStaffController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('vehicles')->name('vehicles/')->group(static function() {
            Route::get('/',                                             'VehiclesController@index')->name('index');
            Route::get('/create',                                       'VehiclesController@create')->name('create');
            Route::post('/',                                            'VehiclesController@store')->name('store');
            Route::get('/{vehicle}/edit',                               'VehiclesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'VehiclesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{vehicle}',                                   'VehiclesController@update')->name('update');
            Route::delete('/{vehicle}',                                 'VehiclesController@destroy')->name('destroy');
            Route::get('/export',                                       'VehiclesController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('transactions')->name('transactions/')->group(static function() {
            Route::get('/',                                             'TransactionsController@index')->name('index');
            Route::get('/create',                                       'TransactionsController@create')->name('create');
            Route::post('/',                                            'TransactionsController@store')->name('store');
            Route::get('/{transaction}/edit',                           'TransactionsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TransactionsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{transaction}',                               'TransactionsController@update')->name('update');
            Route::delete('/{transaction}',                             'TransactionsController@destroy')->name('destroy');
            Route::get('/export',                                       'TransactionsController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('transaction-details')->name('transaction-details/')->group(static function() {
            Route::get('/',                                             'TransactionDetailsController@index')->name('index');
            Route::get('/create',                                       'TransactionDetailsController@create')->name('create');
            Route::post('/',                                            'TransactionDetailsController@store')->name('store');
            Route::get('/{transactionDetail}/edit',                     'TransactionDetailsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TransactionDetailsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{transactionDetail}',                         'TransactionDetailsController@update')->name('update');
            Route::delete('/{transactionDetail}',                       'TransactionDetailsController@destroy')->name('destroy');
            Route::get('/export',                                       'TransactionDetailsController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('complaints')->name('complaints/')->group(static function() {
            Route::get('/',                                             'ComplaintsController@index')->name('index');
            Route::get('/create',                                       'ComplaintsController@create')->name('create');
            Route::post('/',                                            'ComplaintsController@store')->name('store');
            Route::get('/{complaint}/edit',                             'ComplaintsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ComplaintsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{complaint}',                                 'ComplaintsController@update')->name('update');
            Route::delete('/{complaint}',                               'ComplaintsController@destroy')->name('destroy');
            Route::get('/export',                                       'ComplaintsController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('complaint-details')->name('complaint-details/')->group(static function() {
            Route::get('/',                                             'ComplaintDetailsController@index')->name('index');
            Route::get('/create',                                       'ComplaintDetailsController@create')->name('create');
            Route::post('/',                                            'ComplaintDetailsController@store')->name('store');
            Route::get('/{complaintDetail}/edit',                       'ComplaintDetailsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ComplaintDetailsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{complaintDetail}',                           'ComplaintDetailsController@update')->name('update');
            Route::delete('/{complaintDetail}',                         'ComplaintDetailsController@destroy')->name('destroy');
            Route::get('/export',                                       'ComplaintDetailsController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('drivers')->name('drivers/')->group(static function() {
            Route::get('/',                                             'DriversController@index')->name('index');
            Route::get('/create',                                       'DriversController@create')->name('create');
            Route::post('/',                                            'DriversController@store')->name('store');
            Route::get('/{driver}/edit',                                'DriversController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'DriversController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{driver}',                                    'DriversController@update')->name('update');
            Route::delete('/{driver}',                                  'DriversController@destroy')->name('destroy');
            Route::get('/export',                                       'DriversController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('conductors')->name('conductors/')->group(static function() {
            Route::get('/',                                             'ConductorsController@index')->name('index');
            Route::get('/create',                                       'ConductorsController@create')->name('create');
            Route::post('/',                                            'ConductorsController@store')->name('store');
            Route::get('/{conductor}/edit',                             'ConductorsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ConductorsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{conductor}',                                 'ConductorsController@update')->name('update');
            Route::delete('/{conductor}',                               'ConductorsController@destroy')->name('destroy');
            Route::get('/export',                                       'ConductorsController@export')->name('export');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('inspectors')->name('inspectors/')->group(static function() {
            Route::get('/',                                             'InspectorsController@index')->name('index');
            Route::get('/create',                                       'InspectorsController@create')->name('create');
            Route::post('/',                                            'InspectorsController@store')->name('store');
            Route::get('/{inspector}/edit',                             'InspectorsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'InspectorsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{inspector}',                                 'InspectorsController@update')->name('update');
            Route::delete('/{inspector}',                               'InspectorsController@destroy')->name('destroy');
            Route::get('/export',                                       'InspectorsController@export')->name('export');
        });
    });
});