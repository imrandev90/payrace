<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    'organization' => [
        'title' => 'Organizations',

        'actions' => [
            'index' => 'Organizations',
            'create' => 'New Organization',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            'address' => 'Address',
            'contact_name' => 'Contact name',
            'contact_phone' => 'Contact phone',
            
        ],
    ],

    'organization-staff' => [
        'title' => 'Organization Staff',

        'actions' => [
            'index' => 'Organization Staff',
            'create' => 'New Organization Staff',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'full_name' => 'Full name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'designation' => 'Designation',
            'organization_id' => 'Organization',
            'password' => 'Password',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
            
        ],
    ],

    'owner' => [
        'title' => 'Owners',

        'actions' => [
            'index' => 'Owners',
            'create' => 'New Owner',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'full_name' => 'Full name',
            'phone' => 'Phone',
            'email' => 'Email',
            'address' => 'Address',
            
        ],
    ],

    'route' => [
        'title' => 'Routes',

        'actions' => [
            'index' => 'Routes',
            'create' => 'New Route',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            'number' => 'Number',
            'description' => 'Description',
            'distance' => 'Distance',
            
        ],
    ],

    'crew-staff' => [
        'title' => 'Crew Staff',

        'actions' => [
            'index' => 'Crew Staff',
            'create' => 'New Crew Staff',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'full_name' => 'Full name',
            'phone' => 'Phone',
            'address' => 'Address',
            'id_number' => 'Id number',
            'designation' => 'Designation',
            'description' => 'Description',
            'organization_id' => 'Organization',
            
        ],
    ],

    'vehicle' => [
        'title' => 'Vehicles',

        'actions' => [
            'index' => 'Vehicles',
            'create' => 'New Vehicle',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'registration_id' => 'Registration',
            'fleet_number' => 'Fleet number',
            'payment_account' => 'Payment account',
            'manu_date' => 'Manu date',
            'owner_id' => 'Owner',
            'conductor_id' => 'Conductor',
            'driver_id' => 'Driver',
            'inspector_id' => 'Inspector',
            'created_by' => 'Created by',
            
        ],
    ],

    'transaction' => [
        'title' => 'Transactions',

        'actions' => [
            'index' => 'Transactions',
            'create' => 'New Transaction',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'payee_id' => 'Payee',
            'phone' => 'Phone',
            'full_name' => 'Full name',
            'account' => 'Account',
            'paybill' => 'Paybill',
            'amount' => 'Amount',
            'channel' => 'Channel',
            'payment_details' => 'Payment details',
            
        ],
    ],

    'transaction-detail' => [
        'title' => 'Transaction Details',

        'actions' => [
            'index' => 'Transaction Details',
            'create' => 'New Transaction Detail',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'payment_id' => 'Payment',
            'vehicle_id' => 'Vehicle',
            'route_id' => 'Route',
            'conductor_id' => 'Conductor',
            'driver_id' => 'Driver',
            'organization_id' => 'Organization',
            'inspector_id' => 'Inspector',
            'trip_id' => 'Trip',
            
        ],
    ],

    'complaint' => [
        'title' => 'Complaints',

        'actions' => [
            'index' => 'Complaints',
            'create' => 'New Complaint',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'phone' => 'Phone',
            'title' => 'Title',
            'description' => 'Description',
            'handled_by' => 'Handled by',
            'handled_at' => 'Handled at',
            
        ],
    ],

    'complaint-detail' => [
        'title' => 'Complaint Details',

        'actions' => [
            'index' => 'Complaint Details',
            'create' => 'New Complaint Detail',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'complaint_id' => 'Complaint',
            'vehicle_id' => 'Vehicle',
            'route_id' => 'Route',
            'conductor_id' => 'Conductor',
            'driver_id' => 'Driver',
            'organization_id' => 'Organization',
            'inspector_id' => 'Inspector',
            'trip_id' => 'Trip',
            
        ],
    ],

    'driver' => [
        'title' => 'Drivers',

        'actions' => [
            'index' => 'Drivers',
            'create' => 'New Driver',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'full_name' => 'Full name',
            'phone' => 'Phone',
            'address' => 'Address',
            'id_number' => 'Id number',
            'organization_id' => 'Organization',
            
        ],
    ],

    'conductor' => [
        'title' => 'Conductors',

        'actions' => [
            'index' => 'Conductors',
            'create' => 'New Conductor',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'full_name' => 'Full name',
            'phone' => 'Phone',
            'address' => 'Address',
            'id_number' => 'Id number',
            'organization_id' => 'Organization',
            
        ],
    ],

    'inspector' => [
        'title' => 'Inspectors',

        'actions' => [
            'index' => 'Inspectors',
            'create' => 'New Inspector',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'full_name' => 'Full name',
            'phone' => 'Phone',
            'address' => 'Address',
            'id_number' => 'Id number',
            'organization_id' => 'Organization',
            
        ],
    ],

    'driver' => [
        'title' => 'Drivers',

        'actions' => [
            'index' => 'Drivers',
            'create' => 'New Driver',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'full_name' => 'Full name',
            'phone' => 'Phone',
            'address' => 'Address',
            'id_number' => 'Id number',
            'organization_id' => 'Organization',
            
        ],
    ],

    'transaction' => [
        'title' => 'Transactions',

        'actions' => [
            'index' => 'Transactions',
            'create' => 'New Transaction',
            'edit' => 'Edit :name',
            'export' => 'Export',
        ],

        'columns' => [
            'id' => 'ID',
            'payee_id' => 'Payee',
            'phone' => 'Phone',
            'full_name' => 'Full name',
            'account' => 'Account',
            'paybill' => 'Paybill',
            'amount' => 'Amount',
            'channel' => 'Channel',
            'payment_details' => 'Payment details',
            'time' => 'Time',
            'vehicle_id' => 'Vehicle',
            'route_id' => 'Route',
            'conductor_id' => 'Conductor',
            'driver_id' => 'Driver',
            'organization_id' => 'Organization',
            'inspector_id' => 'Inspector',
            'trip_id' => 'Trip',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];