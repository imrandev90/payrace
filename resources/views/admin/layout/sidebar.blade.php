<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">

          <li class="nav-title">Analytics</li>

          <li class="nav-item">
            <a class="nav-link" href="{{ url('admin/dashboard') }}">
              <i class="nav-icon icon-graduation"></i> Dashboard
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ url('admin/reports/vehicles') }}">
              <i class="nav-icon icon-graduation"></i> Reports
            </a>
          </li>

          <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.content') }}</li>
          <li class="nav-item"><a class="nav-link" href="{{ url('admin/organizations') }}"><i class="nav-icon icon-graduation"></i> {{ trans('admin.organization.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/organization-staffs') }}"><i class="nav-icon icon-ghost"></i> {{ trans('admin.organization-staff.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/owners') }}"><i class="nav-icon icon-umbrella"></i> {{ trans('admin.owner.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/routes') }}"><i class="nav-icon icon-compass"></i> {{ trans('admin.route.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/drivers') }}"><i class="nav-icon icon-book-open"></i> {{ trans('admin.driver.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/conductors') }}"><i class="nav-icon icon-puzzle"></i> {{ trans('admin.conductor.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/inspectors') }}"><i class="nav-icon icon-star"></i> {{ trans('admin.inspector.title') }}</a></li>
           {{-- Do not delete me :) I'm used for auto-generation menu items --}}
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/vehicles') }}"><i class="nav-icon icon-energy"></i> {{ trans('admin.vehicle.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/transactions') }}"><i class="nav-icon icon-plane"></i> {{ trans('admin.transaction.title') }}</a></li>

           {{-- <li class="nav-item"><a class="nav-link" href="{{ url('admin/transaction-details') }}"><i class="nav-icon icon-ghost"></i> {{ trans('admin.transaction-detail.title') }}</a></li> --}}
           
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/complaints') }}"><i class="nav-icon icon-umbrella"></i> {{ trans('admin.complaint.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/complaint-details') }}"><i class="nav-icon icon-flag"></i> {{ trans('admin.complaint-detail.title') }}</a></li>
           


            <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.settings') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/admin-users') }}"><i class="nav-icon icon-user"></i> {{ __('Manage access') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/translations') }}"><i class="nav-icon icon-location-pin"></i> {{ __('Translations') }}</a></li>

            
            {{-- Do not delete me :) I'm also used for auto-generation menu items --}}
            {{--<li class="nav-item"><a class="nav-link" href="{{ url('admin/configuration') }}"><i class="nav-icon icon-settings"></i> {{ __('Configuration') }}</a></li>--}}
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
