<div class="form-group row align-items-center" :class="{'has-danger': errors.has('payee_id'), 'has-success': fields.payee_id && fields.payee_id.valid }">
    <label for="payee_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.payee_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.payee_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('payee_id'), 'form-control-success': fields.payee_id && fields.payee_id.valid}" id="payee_id" name="payee_id" placeholder="{{ trans('admin.transaction.columns.payee_id') }}">
        <div v-if="errors.has('payee_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('payee_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('phone'), 'has-success': fields.phone && fields.phone.valid }">
    <label for="phone" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.phone') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.phone" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('phone'), 'form-control-success': fields.phone && fields.phone.valid}" id="phone" name="phone" placeholder="{{ trans('admin.transaction.columns.phone') }}">
        <div v-if="errors.has('phone')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('phone') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('full_name'), 'has-success': fields.full_name && fields.full_name.valid }">
    <label for="full_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.full_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.full_name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('full_name'), 'form-control-success': fields.full_name && fields.full_name.valid}" id="full_name" name="full_name" placeholder="{{ trans('admin.transaction.columns.full_name') }}">
        <div v-if="errors.has('full_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('full_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('account'), 'has-success': fields.account && fields.account.valid }">
    <label for="account" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.account') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.account" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('account'), 'form-control-success': fields.account && fields.account.valid}" id="account" name="account" placeholder="{{ trans('admin.transaction.columns.account') }}">
        <div v-if="errors.has('account')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('account') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('paybill'), 'has-success': fields.paybill && fields.paybill.valid }">
    <label for="paybill" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.paybill') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.paybill" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('paybill'), 'form-control-success': fields.paybill && fields.paybill.valid}" id="paybill" name="paybill" placeholder="{{ trans('admin.transaction.columns.paybill') }}">
        <div v-if="errors.has('paybill')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('paybill') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('amount'), 'has-success': fields.amount && fields.amount.valid }">
    <label for="amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.amount') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.amount" v-validate="'required|decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('amount'), 'form-control-success': fields.amount && fields.amount.valid}" id="amount" name="amount" placeholder="{{ trans('admin.transaction.columns.amount') }}">
        <div v-if="errors.has('amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('amount') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('channel'), 'has-success': fields.channel && fields.channel.valid }">
    <label for="channel" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.channel') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.channel" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('channel'), 'form-control-success': fields.channel && fields.channel.valid}" id="channel" name="channel" placeholder="{{ trans('admin.transaction.columns.channel') }}">
        <div v-if="errors.has('channel')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('channel') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('payment_details'), 'has-success': fields.payment_details && fields.payment_details.valid }">
    <label for="payment_details" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.payment_details') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.payment_details" v-validate="''" id="payment_details" name="payment_details"></textarea>
        </div>
        <div v-if="errors.has('payment_details')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('payment_details') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('time'), 'has-success': fields.time && fields.time.valid }">
    <label for="time" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.time') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.time" :config="datetimePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('time'), 'form-control-success': fields.time && fields.time.valid}" id="time" name="time" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('time')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('time') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('vehicle_id'), 'has-success': fields.vehicle_id && fields.vehicle_id.valid }">
    <label for="vehicle_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.vehicle_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.vehicle_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vehicle_id'), 'form-control-success': fields.vehicle_id && fields.vehicle_id.valid}" id="vehicle_id" name="vehicle_id" placeholder="{{ trans('admin.transaction.columns.vehicle_id') }}">
        <div v-if="errors.has('vehicle_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('route_id'), 'has-success': fields.route_id && fields.route_id.valid }">
    <label for="route_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.route_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.route_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('route_id'), 'form-control-success': fields.route_id && fields.route_id.valid}" id="route_id" name="route_id" placeholder="{{ trans('admin.transaction.columns.route_id') }}">
        <div v-if="errors.has('route_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('route_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('conductor_id'), 'has-success': fields.conductor_id && fields.conductor_id.valid }">
    <label for="conductor_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.conductor_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.conductor_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('conductor_id'), 'form-control-success': fields.conductor_id && fields.conductor_id.valid}" id="conductor_id" name="conductor_id" placeholder="{{ trans('admin.transaction.columns.conductor_id') }}">
        <div v-if="errors.has('conductor_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('conductor_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('driver_id'), 'has-success': fields.driver_id && fields.driver_id.valid }">
    <label for="driver_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.driver_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.driver_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('driver_id'), 'form-control-success': fields.driver_id && fields.driver_id.valid}" id="driver_id" name="driver_id" placeholder="{{ trans('admin.transaction.columns.driver_id') }}">
        <div v-if="errors.has('driver_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('driver_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('organization_id'), 'has-success': fields.organization_id && fields.organization_id.valid }">
    <label for="organization_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.organization_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.organization_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('organization_id'), 'form-control-success': fields.organization_id && fields.organization_id.valid}" id="organization_id" name="organization_id" placeholder="{{ trans('admin.transaction.columns.organization_id') }}">
        <div v-if="errors.has('organization_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('organization_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('inspector_id'), 'has-success': fields.inspector_id && fields.inspector_id.valid }">
    <label for="inspector_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.inspector_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.inspector_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('inspector_id'), 'form-control-success': fields.inspector_id && fields.inspector_id.valid}" id="inspector_id" name="inspector_id" placeholder="{{ trans('admin.transaction.columns.inspector_id') }}">
        <div v-if="errors.has('inspector_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('inspector_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('trip_id'), 'has-success': fields.trip_id && fields.trip_id.valid }">
    <label for="trip_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transaction.columns.trip_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.trip_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('trip_id'), 'form-control-success': fields.trip_id && fields.trip_id.valid}" id="trip_id" name="trip_id" placeholder="{{ trans('admin.transaction.columns.trip_id') }}">
        <div v-if="errors.has('trip_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('trip_id') }}</div>
    </div>
</div>


