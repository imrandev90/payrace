@extends('brackets/admin-ui::admin.layout.default')

@section('title', "Dashboard")

@section("styles")
<link rel="stylesheet" href="https://unpkg.com/@coreui/coreui@3.0.0-rc.0/dist/css/coreui.min.css">

<style>
canvas {
-moz-user-select: none;
-webkit-user-select: none;
-ms-user-select: none;
}
</style>

@stop

@section('body')
    <dashboard-view
    	{{-- :pending_count="'{{ ($pendingCount) }}'"
    	:logs="{{ json_encode($logs) }}" --}}
    	v-cloak
        inline-template>

        <div>

            <div class="row">
                <div class="col-6">
                    <h4>
                        @{{ todayString }}
                    </h4>
                </div>

                {{-- <h5 style="
                    margin-right: 15px;
                    margin-left: 15px;
                    margin-top: 5px; 
                    margin-bottom: 5px;
                    ">
                    From
                </h5>

                <datetime
                class="col-2 pull-left m-b-0" 
                style="background: white;"
                v-model="startDate" 
                :config="config" 
                class="flatpickr" 
                id="startDate" 
                name="startDate" 
                placeholder="Start date">
                    
                </datetime>

                <h5 
                style="
                margin-right: 15px;
                margin-left: 15px;
                margin-top: 5px; 
                margin-bottom: 5px;
                ">
                    To
                </h5>

                <datetime 
                class="col-2 pull-right m-b-0"
                style="background: white;" 
                v-model="endDate" 
                :config="config" 
                class="flatpickr" 
                id="endDate" 
                name="endDate" 
                placeholder="End date">
                    
                </datetime>

                <div class="col-1">
                    <a 
                    class="btn btn-primary btn-spinner pull-right m-b-0" 
                    style="margin-left: 8px; background: #1f476e; color: white;"
                    href="#"
                    @click="updateDashboard()"
                    role="button">
                        Update
                    </a>
                </div> --}}
            </div>

            <div class="row">
              <div class="col-sm-2">
                <div 
                style="background: white; padding-top: 16px; padding-bottom: 16px;" 
                class="c-callout c-callout-info b-t-4 b-r-1 b-b-1">
                  <h6 class="text-muted">Todays Earnings</h6><br>
                  <strong class="h4">{{ "KES 675,600" }}</strong>
                </div>
              </div>


              <div class="col-sm-2">
                <div
                style="background: white; padding-top: 16px; padding-bottom: 16px;"
                class="c-callout c-callout-warning b-t-1 b-r-1 b-b-1">
                  <h6 class="text-muted">Earnings Since Monday</h6><br>
                  <strong class="h4">{{ "KES 1,250,600" }}</strong>
                </div>
              </div>


               <div class="col-sm-2">
                <div
                style="background: white; padding-top: 16px; padding-bottom: 16px;"
                class="c-callout c-callout-danger b-t-1 b-r-1 b-b-1">
                  <h6 class="text-muted">This Month Earnings</h6><br>
                  <strong class="h4">{{ "KES 11,250,600" }}</strong>
                </div>
              </div>


              <div class="col-sm-2">
                <div 
                style="background: white; padding-top: 16px; padding-bottom: 16px;"
                class="c-callout c-callout-success b-t-1 b-r-1 b-b-1">
                  <h6 class="text-muted">This Month Complaints</h6><br>
                  <strong class="h4">{{ "600" }}</strong>
                </div>
              </div>

              <div class="col-sm-2">
                <div 
                style="background: white; padding-top: 16px; padding-bottom: 16px;"
                class="c-callout c-callout-primary b-t-1 b-r-1 b-b-1">
                  <h6 class="text-muted">Average Processing Time</h6><br>
                  <strong class="h4">{{ "5 secs" }}</strong>
                </div>
              </div>

            
              <div class="col-sm-2">
                <div 
                style="background: white; padding-top: 16px; padding-bottom: 16px;"
                class="c-callout b-t-1 b-r-1 b-b-1">
                  <h6 class="text-muted">Downtimes</h6><br>
                  <strong class="h4">{{ 1 }}</strong>
                </div>
              </div>

              
            </div>



            <div class="row">
                <div class="col-6">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-header">
                            <h4>Highest Earning Vehicles</h4>
                        </div>

                        <div class="card-body" v-cloak style="margin: 0px; padding: 0px;">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Reg No</th>
                                        <th>Fleet No</th>
                                        <th>Amount</th>
                                        <th>Transactions</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                     @foreach($vehicles as $vehicle)
                                    <tr>
                                        <td>{{ $vehicle->id }}</td>
                                        <td>{{ $vehicle->registration_id }}</td>
                                        <td>{{ $vehicle->fleet_no }}</td>
                                        <td>{{ rand(80000, 150000) }}</td>
                                        <td>{{ rand(15000, 50000) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                       {{--  <div class="card-footer">
                        </div> --}}
                    </div>
                </div>


                <div class="col-6">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-header">
                            <h4>Highest Earning Routes</h4>
                        </div>

                        <div class="card-body" v-cloak style="margin: 0px; padding: 0px;">                      
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Number</th>
                                        <th>Amount</th>
                                        <th>Transactions</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    @foreach($routes as $route)
                                    <tr>
                                        <td>{{ $route->id }}</td>
                                        <td>{{ $route->title }}</td>
                                        <td>{{ $route->number }}</td>
                                        <td>{{ rand(100000, 500000) }}</td>
                                        <td>{{ rand(10000, 50000) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-8">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-header">
                            <h4>Recent Transactions</h4>
                        </div>

                        <div class="card-body" v-cloak style="margin: 0px; padding: 0px;">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Payee ID</th>
                                        <th>Phone</th>
                                        <th>Name</th>
                                        <th>Channel</th>
                                        <th>Account No</th>
                                        <th>Paybill</th>
                                        <th>Amount</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    @foreach($transactions as $transaction)
                                    <tr>
                                        <td>{{ $transaction->id }}</td>
                                        <td>{{ $transaction->payee_id }}</td>
                                        <td>{{ $transaction->phone }}</td>
                                        <td>{{ $transaction->full_name }}</td>
                                        <td>{{ $transaction->channel }}</td>
                                        <td>{{ $transaction->account }}</td>
                                        <td>{{ $transaction->paybill }}</td>
                                        <td>{{ rand(2,10)*10 }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                       {{--  <div class="card-footer">
                        </div> --}}
                    </div>
                </div>


                <div class="col-4">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-header">
                            <h4>Earning (KSH) by Route: Today</h4>
                        </div>

                        <div class="card-body" v-cloak style="margin: 0px; padding: 0px;"> 
                            <canvas id="pie-chart" width="100%" height="100%"></canvas>
                        </div>

                    </div>
                </div>

            </div>

            
        </div>




    </dashboard-view>

    @section("bottom-scripts")
    <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>

    <script>
      $(function(){
          //get the pie chart canvas
          var cData = JSON.parse(`<?php echo $chart_data; ?>`);
          var ctx = $("#pie-chart");
     
          //pie chart data
          var data = {
            labels: cData.label,
            datasets: [
              {
                label: "Total Earnings",
                data: cData.data,
                backgroundColor: [
                  "#DEB887",
                  "#A9A9A9",
                  "#DC143C",
                  "#F4A460",
                  "#2E8B57",
                  "#1D7A46",
                  "#CDA776",
                ],

                borderColor: [
                  "#CDA776",
                  "#989898",
                  "#CB252B",
                  "#E39371",
                  "#1D7A46",
                  "#F4A460",
                  "#CDA776",
                ],

                borderWidth: [1, 1, 1, 1, 1,1,1]
              }
            ]
          };
     
          //options
          var options = {
            responsive: true,
            title: {
              display: true,
              position: "top",
              text: "Earning (KSH) by Route -  Today",
              fontSize: 18,
              fontColor: "#111"
            },


            legend: {
              display: true,
              position: "bottom",
              labels: {
                fontColor: "#333",
                fontSize: 16
              }
            }

          };
     
          //create Pie Chart class object
          var chart1 = new Chart(ctx, {
            type: "pie",
            data: data,
            options: options
          });
     
      });
    </script>


    <script src="https://unpkg.com/@coreui/coreui@3.0.0-rc.0/dist/js/coreui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js" integrity="sha384-L2pyEeut/H3mtgCBaUNw7KWzp5n9&#43;4pDQiExs933/5QfaTh8YStYFFkOzSoXjlTb" crossorigin="anonymous"></script>
    @stop
@endsection