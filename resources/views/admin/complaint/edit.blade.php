@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.complaint.actions.edit', ['name' => $complaint->title]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <complaint-form
                :action="'{{ $complaint->resource_url }}'"
                :data="{{ $complaint->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.complaint.actions.edit', ['name' => $complaint->title]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.complaint.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </complaint-form>

        </div>
    
</div>

@endsection