@extends('brackets/admin-ui::admin.layout.default')

@section('title', "Reports")

@section("styles")
<link rel="stylesheet" href="https://unpkg.com/@coreui/coreui@3.0.0-rc.0/dist/css/coreui.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<style>
  #vehicles-table tbody tr:hover,  #vehicles-table tbody tr.selected {
    background-color: #445165;
    color: white;
  }


  #owners-table tbody tr:hover,  #owners-table tbody tr.selected {
    background-color: #445165;
    color: white;
  }

  #routes-table tbody tr:hover,  #routes-table tbody tr.selected {
    background-color: #445165;
    color: white;
  }

  #drivers-table tbody tr:hover,  #drivers-table tbody tr.selected {
    background-color: #445165;
    color: white;
  }

  td{
    text-align:left;
  }


  #vehicles-search {
    /*background-image: url('/css/searchicon.png'); */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
  }

  #owners-search {
    /*background-image: url('/css/searchicon.png'); */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
  }

  #routes-search {
    /*background-image: url('/css/searchicon.png'); */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
  }

  #drivers-search {
    /*background-image: url('/css/searchicon.png'); */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
  }


  #conductors-search {
    /*background-image: url('/css/searchicon.png'); */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
  }

</style>
@stop

@section('body')
    <reports-view
    	{{-- :pending_count="'{{ ($pendingCount) }}'"
    	:logs="{{ json_encode($logs) }}" --}}
    	v-cloak
      inline-template>
        <div>
          <div>
            <nav class="nav nav-pills nav-justified" style="margin-bottom: 20px;background: white;">
              <a
              class="nav-item nav-link {{ \Route::is('admin/reports/vehicles') ? 'active' : '' }}" 
              href="{{ url('admin/reports/vehicles') }}">Vehicles</a>

              <a 
              class="nav-item nav-link {{ \Route::is('admin/reports/owners') ? 'active' : '' }}" 
              href="{{ url('admin/reports/owners') }}">Owners</a>

              <a 
              class="nav-item nav-link {{ \Route::is('admin/reports/routes') ? 'active' : '' }}" 
              href="{{ url('admin/reports/routes') }}">Routes</a>

              <a 
              class="nav-item nav-link {{ \Route::is('admin/reports/drivers') ? 'active' : '' }}" 
              href="{{ url('admin/reports/drivers') }}">Drivers</a>

              <a class="nav-item nav-link {{ \Route::is('admin/reports/conductors') ? 'active' : '' }}" 
              href="{{ url('admin/reports/conductors') }}">Conductor</a>
            </nav>
          <div>
          

          @if(\Route::is('admin/reports/vehicles'))
          <div class="row">
              <div class="col-3">
                  <div class="card" style="min-height: 400px;">
                      <div class="card-header">
                          <h4>List of Vehicles</h4>
                      </div>

                      <div class="card-body" v-cloak style="margin: 0px; padding: 0px;">
                          <input 
                          type="text" 
                          id="vehicles-search" 
                          onkeyup="searchVehicle()" 
                          placeholder="Search vehicle..">

                          <table class="table table-hover table-listing" id="vehicles-table">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Reg No</th>
                                      <th>Fleet No</th>
                                      {{-- <th>Action</th> --}}
                                  </tr>
                                  
                              </thead>
                              <tbody>
                                  @foreach($vehicles as $vehicle)
                                  <tr 
                                  id="{{ $vehicle->id }}"
                                  class="{{ app('request')->input('id') == $vehicle->id ? 'selected' : '' }}">
                                      <td>{{ $vehicle->id }}</td>
                                      <td>{{ $vehicle->registration_id }}</td>
                                      <td>{{ $vehicle->fleet_number }}</td>
                                      {{-- <td></td> --}}
                                  </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>

                      
                  </div>
              </div>

              @if(app('request')->input('id'))
              <div class="col-9">
                  <div class="card" style="min-height: 400px;">
                      <div class="card-header">
                          <h4>Vehicle Transaction Report</h4>
                      </div>

                      <div class="card-body" v-cloak style="margin: 0px; padding: 0px;">   
                          <div
                          style="margin: 10px;"
                          class="row">

                            <div class="col-4">
                                <vue-segmented-control
                                style="background: #fff;height:40px;"
                                :selected="selected_control"
                                :options="controls"
                                label="label"
                                value="value"
                                color="#fff"
                                active-color="#485779"
                                :multiple="false"
                                :enabled="true"
                                @select="onSelect"/>
                            </div>


                            <h5 
                            v-if="showFilter"
                            style="
                            margin-right: 15px;
                            margin-left: 15px;
                            margin-top: 5px; 
                            margin-bottom: 5px;
                            ">
                              From
                            </h5>

                            <datetime
                            v-if="showFilter"
                            class="col-2 pull-left m-b-0" 
                            style="background: white;"
                            v-model="startDate" 
                            :config="config" 
                            class="flatpickr" 
                            id="startDate" 
                            name="startDate" 
                            placeholder="Start date">
                                    
                            </datetime>

                            <h5 
                            v-if="showFilter"
                            style="
                            margin-right: 15px;
                            margin-left: 15px;
                            margin-top: 5px; 
                            margin-bottom: 5px;">
                                  To
                            </h5>

                            <datetime
                            v-if="showFilter"
                            class="col-2 pull-right m-b-0"
                            style="background: white;" 
                            v-model="endDate" 
                            :config="config" 
                            class="flatpickr" 
                            id="endDate" 
                            name="endDate" 
                            placeholder="End date">
                                  
                            </datetime>

                            <div
                            v-if="showFilter" 
                            class="col-1">
                            <button 
                            class="btn btn-primary btn-spinner pull-right m-b-0" 
                            style="margin-left: 8px; background: #1f476e; color: white;"
                            @click="updateReport()"
                            role="button">
                                  Filter
                              </button>
                            </div>

                               
                          </div>



                          <canvas id="vehicle-bar-chart" width="100%" height="50%"></canvas>
                      </div>

                      <div class="card-footer">
                        @php
                          $vehicle = App\Models\Vehicle::find(app('request')->input('id'));
                        @endphp


                        


                        <table class="table table-listing">
                                <thead>
                                    <tr style="pointer-events:none">
                                        <th>{{ trans('admin.vehicle.columns.registration_id') }}</th>
                                        <th>{{ trans('admin.vehicle.columns.fleet_number') }}</th>
                                        <th>{{ trans('admin.vehicle.columns.payment_account') }}</th>
                                        <th>{{ trans('admin.vehicle.columns.manu_date') }}</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    <tr style="pointer-events:none">
                                        <td>{{ $vehicle->registration_id }}</td>
                                        <td>{{ $vehicle->fleet_number }}</td>
                                        <td>{{ $vehicle->payment_account }}</td>
                                        <td>{{ $vehicle->manu_date }}</td>
                                    </tr>
                                </tbody>
                            </table>
                      </div>

                  </div>
              </div>
              @endif

            </div>
            @endif

            @if(\Route::is('admin/reports/owners'))
            <div class="row">
                <div class="col-3">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-header">
                            <h4>List of Owners</h4>
                        </div>

                        <div class="card-body" v-cloak style="margin: 0px; padding: 0px;">
                            <input 
                            type="text" 
                            id="owners-search" 
                            onkeyup="searchOwner()" 
                            placeholder="Search owner..">

                            <table class="table table-hover table-listing" id="owners-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        {{-- <th>Action</th> --}}
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    @foreach($owners as $owner)
                                    <tr 
                                    id="{{ $owner->id }}"
                                    class="{{ app('request')->input('id') == $owner->id ? 'selected' : '' }}">
                                        <td>{{ $owner->id }}</td>
                                        <td>{{ $owner->full_name }}</td>
                                        <td>{{ $owner->phone }}</td>
                                        {{-- <td></td> --}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                       {{--  <div class="card-footer">
                        </div> --}}
                    </div>
                </div>

                @if(app('request')->input('id'))
                <div class="col-9">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-header">
                            <h4>Owners Transaction Report</h4>
                        </div>

                        <div class="card-body" v-cloak style="margin: 0px; padding: 0px;"> 
                            <div
                          style="margin: 10px;"
                          class="row">

                            <div class="col-4">
                                <vue-segmented-control
                                style="background: #fff;height:40px;"
                                :selected="selected_control"
                                :options="controls"
                                label="label"
                                value="value"
                                color="#fff"
                                active-color="#485779"
                                :multiple="false"
                                :enabled="true"
                                @select="onSelect"/>
                            </div>


                            <h5 
                            v-if="showFilter"
                            style="
                            margin-right: 15px;
                            margin-left: 15px;
                            margin-top: 5px; 
                            margin-bottom: 5px;
                            ">
                              From
                            </h5>

                            <datetime
                            v-if="showFilter"
                            class="col-2 pull-left m-b-0" 
                            style="background: white;"
                            v-model="startDate" 
                            :config="config" 
                            class="flatpickr" 
                            id="startDate" 
                            name="startDate" 
                            placeholder="Start date">
                                    
                            </datetime>

                            <h5 
                            v-if="showFilter"
                            style="
                            margin-right: 15px;
                            margin-left: 15px;
                            margin-top: 5px; 
                            margin-bottom: 5px;">
                                  To
                            </h5>

                            <datetime
                            v-if="showFilter"
                            class="col-2 pull-right m-b-0"
                            style="background: white;" 
                            v-model="endDate" 
                            :config="config" 
                            class="flatpickr" 
                            id="endDate" 
                            name="endDate" 
                            placeholder="End date">
                                  
                            </datetime>

                            <div
                            v-if="showFilter" 
                            class="col-1">
                            <button 
                            class="btn btn-primary btn-spinner pull-right m-b-0" 
                            style="margin-left: 8px; background: #1f476e; color: white;"
                            @click="updateReport()"
                            role="button">
                                  Filter
                              </button>
                            </div>

                               
                          </div>                     
                          <canvas id="owner-bar-chart" width="100%" height="50%"></canvas>
                        </div>

                        <div class="card-footer">
                          @php
                            $owner = App\Models\Owner::find(app('request')->input('id'));
                          @endphp

                            <table class="table table-listing">
                                <thead>
                                    <tr style="pointer-events:none">
                                      <th>{{ trans('admin.owner.columns.full_name') }}</th>
                                      <th>{{ trans('admin.owner.columns.phone') }}</th>
                                      <th>{{ trans('admin.owner.columns.email') }}</th>
                                      <th>{{ trans('admin.owner.columns.address') }}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr style="pointer-events:none">
                                        <td>{{ $owner->full_name }}</td>
                                        <td>{{ $owner->phone }}</td>
                                        <td>{{ $owner->email }}</td>
                                        <td>{{ $owner->address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                          </div>


                        </div>

                    </div>
                </div>
                @endif

            </div>
            @endif




            @if(\Route::is('admin/reports/routes'))
            <div class="row">
                <div class="col-3">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-header">
                            <h4>List of Routes</h4>
                        </div>

                        <div class="card-body" v-cloak style="margin: 0px; padding: 0px;">
                            <input 
                            type="text" 
                            id="routes-search" 
                            onkeyup="searchRoute()" 
                            placeholder="Search routes..">

                            <table class="table table-hover table-listing">
                                <table class="table table-hover table-listing" id="routes-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Number</th>
                                        {{-- <th>Action</th>= --}}
                                    </tr>
                                    
                                </thead>
                                <tbody>

                                    @foreach($routes as $route)
                                    <tr 
                                    id="{{ $route->id }}" 
                                    class="{{ app('request')->input('id') == $route->id ? 'selected' : '' }}">
                                        <td>{{ $route->id }}</td>
                                        <td>{{ $route->title }}</td>
                                        <td>{{ $route->number }}</td>
                                        {{-- <td></td> --}}
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>
                            </table>
                        </div>

                       {{--  <div class="card-footer">
                        </div> --}}
                    </div>
                </div>


                @if(app('request')->input('id'))
                <div class="col-9">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-header">
                            <h4>Routes Transaction Report</h4>
                        </div>

                        <div class="card-body" v-cloak style="margin: 0px; padding: 0px;">  
                          <div
                          style="margin: 10px;"
                          class="row">

                            <div class="col-4">
                                <vue-segmented-control
                                style="background: #fff;height:40px;"
                                :selected="selected_control"
                                :options="controls"
                                label="label"
                                value="value"
                                color="#fff"
                                active-color="#485779"
                                :multiple="false"
                                :enabled="true"
                                @select="onSelect"/>
                            </div>


                            <h5 
                            v-if="showFilter"
                            style="
                            margin-right: 15px;
                            margin-left: 15px;
                            margin-top: 5px; 
                            margin-bottom: 5px;
                            ">
                              From
                            </h5>

                            <datetime
                            v-if="showFilter"
                            class="col-2 pull-left m-b-0" 
                            style="background: white;"
                            v-model="startDate" 
                            :config="config" 
                            class="flatpickr" 
                            id="startDate" 
                            name="startDate" 
                            placeholder="Start date">
                                    
                            </datetime>

                            <h5 
                            v-if="showFilter"
                            style="
                            margin-right: 15px;
                            margin-left: 15px;
                            margin-top: 5px; 
                            margin-bottom: 5px;">
                                  To
                            </h5>

                            <datetime
                            v-if="showFilter"
                            class="col-2 pull-right m-b-0"
                            style="background: white;" 
                            v-model="endDate" 
                            :config="config" 
                            class="flatpickr" 
                            id="endDate" 
                            name="endDate" 
                            placeholder="End date">
                                  
                            </datetime>

                            <div
                            v-if="showFilter" 
                            class="col-1">
                            <button 
                            class="btn btn-primary btn-spinner pull-right m-b-0" 
                            style="margin-left: 8px; background: #1f476e; color: white;"
                            @click="updateReport()"
                            role="button">
                                  Filter
                              </button>
                            </div>

                               
                          </div>                    
                          <canvas id="route-bar-chart" width="100%" height="50%"></canvas>
                        </div>

                        <div class="card-footer">
                          @php
                            $route = App\Models\Route::find(app('request')->input('id'));
                          @endphp

                          <table class="table table-listing">
                                <thead>
                                    <tr style="pointer-events:none">
                                      <th>{{ trans('admin.route.columns.title') }}</th>
                                      <th>{{ trans('admin.route.columns.number') }}</th>
                                      <th>{{ trans('admin.route.columns.distance') }}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr style="pointer-events:none">
                                        <td>{{ $route->title }}</td>
                                        <td>{{ $route->number }}</td>
                                        <td>{{ $route->distance }}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
                @endif

            </div>
            @endif

            @if(\Route::is('admin/reports/drivers'))
            <p>Drivers</p>
            @endif

            @if(\Route::is('admin/reports/conductors'))
            <p>Conductors</p>
            @endif


        </div>

    </reports-view>

    @section("bottom-scripts")

    <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>

    <script>
      $(function(){
          //get the pie chart canvas
          var cData = JSON.parse(`<?php echo $vehicle_data; ?>`);
          var ctx = $("#vehicle-bar-chart");
     
          //pie chart data
          var data = {
            labels: cData.label,
            datasets: [
              {
                label: "Total Earnings",
                data: cData.data,
                borderWidth: [1, 1, 1, 1, 1,1,1]
              }
            ]
          };
     
          //options
          var options = {
            responsive: true,
            title: {
              display: true,
              position: "top",
              text: "Earning (KSH) by Vehicle -  Today",
              fontSize: 18,
              fontColor: "#111"
            },


            legend: {
              display: true,
              position: "bottom",
              labels: {
                fontColor: "#333",
                fontSize: 16
              }
            }

          };
     
          //create Pie Chart class object
          var chart1 = new Chart(ctx, {
            type: "bar",
            data: data,
            options: options
          });
     
      });
    </script>







    <script>
      $(function(){
          //get the pie chart canvas
          var cData = JSON.parse(`<?php echo $owner_data; ?>`);
          var ctx = $("#owner-bar-chart");
     
          //pie chart data
          var data = {
            labels: cData.label,
            datasets: [
              {
                label: "Total Earnings",
                data: cData.data,
                borderWidth: [1, 1, 1, 1, 1,1,1]
              }
            ]
          };
     
          //options
          var options = {
            responsive: true,
            title: {
              display: true,
              position: "top",
              text: "Earning (KSH) by Owner -  Today",
              fontSize: 18,
              fontColor: "#111"
            },


            legend: {
              display: true,
              position: "bottom",
              labels: {
                fontColor: "#333",
                fontSize: 16
              }
            }

          };
     
          //create Pie Chart class object
          var chart1 = new Chart(ctx, {
            type: "bar",
            data: data,
            options: options
          });
     
      });
    </script>




    <script>
      $(function(){
          //get the pie chart canvas
          var cData = JSON.parse(`<?php echo $route_data; ?>`);
          var ctx = $("#route-bar-chart");
     
          //pie chart data
          var data = {
            labels: cData.label,
            datasets: [
              {
                label: "Total Earnings",
                data: cData.data,
                borderWidth: [1, 1, 1, 1, 1,1,1]
              }
            ]
          };
     
          //options
          var options = {
            responsive: true,
            title: {
              display: true,
              position: "top",
              text: "Earning (KSH) by Route -  Today",
              fontSize: 18,
              fontColor: "#111"
            },


            legend: {
              display: true,
              position: "bottom",
              labels: {
                fontColor: "#333",
                fontSize: 16
              }
            }

          };
     
          //create Pie Chart class object
          var chart1 = new Chart(ctx, {
            type: "bar",
            data: data,
            options: options
          });
     
      });
    </script>

    <script type="text/javascript">
      $("tr").click(function(){
          $(this).addClass("selected").siblings().removeClass("selected");

          var url = window.location.href.split('?')[0];;  

          if (url.indexOf('?') > -1){
             url += '&id='+this.id
          } else{
             url += '?id='+this.id
          }

          window.location.href = url;
      });
    </script>


    <script>
    function searchVehicle() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("vehicles-search");
      filter = input.value.toUpperCase();
      table = document.getElementById("vehicles-table");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }

    function searchOwner() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("owners-search");
      filter = input.value.toUpperCase();
      table = document.getElementById("owners-table");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }

    function searchRoute() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("routes-search");
      filter = input.value.toUpperCase();
      table = document.getElementById("routes-table");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }

    function searchDriver() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("drivers-search");
      filter = input.value.toUpperCase();
      table = document.getElementById("drivers-table");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }


    function searchConductor() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("conductors-search");
      filter = input.value.toUpperCase();
      table = document.getElementById("conductors-table");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }

    </script>






  <script type="text/javascript">
  $(function() {
      var start = moment().subtract(29, 'days');
      var end = moment();

      function cb(start, end) {
          $('#vehicle-date-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          $('#owner-date-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          $('#route-date-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          $('#driver-date-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          $('#conductor-date-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#vehicle-date-range').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, cb);

      cb(start, end);

  });
  </script>



    <script src="https://unpkg.com/@coreui/coreui@3.0.0-rc.0/dist/js/coreui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js" integrity="sha384-L2pyEeut/H3mtgCBaUNw7KWzp5n9&#43;4pDQiExs933/5QfaTh8YStYFFkOzSoXjlTb" crossorigin="anonymous"></script>


    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    @stop
@endsection