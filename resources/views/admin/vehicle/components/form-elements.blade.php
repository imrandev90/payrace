<div class="form-group row align-items-center" :class="{'has-danger': errors.has('registration_id'), 'has-success': fields.registration_id && fields.registration_id.valid }">
    <label for="registration_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.registration_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.registration_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('registration_id'), 'form-control-success': fields.registration_id && fields.registration_id.valid}" id="registration_id" name="registration_id" placeholder="{{ trans('admin.vehicle.columns.registration_id') }}">
        <div v-if="errors.has('registration_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('registration_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fleet_number'), 'has-success': fields.fleet_number && fields.fleet_number.valid }">
    <label for="fleet_number" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.fleet_number') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.fleet_number" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('fleet_number'), 'form-control-success': fields.fleet_number && fields.fleet_number.valid}" id="fleet_number" name="fleet_number" placeholder="{{ trans('admin.vehicle.columns.fleet_number') }}">
        <div v-if="errors.has('fleet_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fleet_number') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('payment_account'), 'has-success': fields.payment_account && fields.payment_account.valid }">
    <label for="payment_account" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.payment_account') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.payment_account" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('payment_account'), 'form-control-success': fields.payment_account && fields.payment_account.valid}" id="payment_account" name="payment_account" placeholder="{{ trans('admin.vehicle.columns.payment_account') }}">
        <div v-if="errors.has('payment_account')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('payment_account') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('manu_date'), 'has-success': fields.manu_date && fields.manu_date.valid }">
    <label for="manu_date" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.manu_date') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.manu_date" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('manu_date'), 'form-control-success': fields.manu_date && fields.manu_date.valid}" id="manu_date" name="manu_date" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('manu_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('manu_date') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('owner_id'), 'has-success': fields.owner_id && fields.owner_id.valid }">
    <label for="owner_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.owner_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.owner_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('owner_id'), 'form-control-success': fields.owner_id && fields.owner_id.valid}" id="owner_id" name="owner_id" placeholder="{{ trans('admin.vehicle.columns.owner_id') }}">
        <div v-if="errors.has('owner_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('owner_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('conductor_id'), 'has-success': fields.conductor_id && fields.conductor_id.valid }">
    <label for="conductor_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.conductor_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.conductor_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('conductor_id'), 'form-control-success': fields.conductor_id && fields.conductor_id.valid}" id="conductor_id" name="conductor_id" placeholder="{{ trans('admin.vehicle.columns.conductor_id') }}">
        <div v-if="errors.has('conductor_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('conductor_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('driver_id'), 'has-success': fields.driver_id && fields.driver_id.valid }">
    <label for="driver_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.driver_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.driver_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('driver_id'), 'form-control-success': fields.driver_id && fields.driver_id.valid}" id="driver_id" name="driver_id" placeholder="{{ trans('admin.vehicle.columns.driver_id') }}">
        <div v-if="errors.has('driver_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('driver_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('inspector_id'), 'has-success': fields.inspector_id && fields.inspector_id.valid }">
    <label for="inspector_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.inspector_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.inspector_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('inspector_id'), 'form-control-success': fields.inspector_id && fields.inspector_id.valid}" id="inspector_id" name="inspector_id" placeholder="{{ trans('admin.vehicle.columns.inspector_id') }}">
        <div v-if="errors.has('inspector_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('inspector_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_by'), 'has-success': fields.created_by && fields.created_by.valid }">
    <label for="created_by" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.vehicle.columns.created_by') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.created_by" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('created_by'), 'form-control-success': fields.created_by && fields.created_by.valid}" id="created_by" name="created_by" placeholder="{{ trans('admin.vehicle.columns.created_by') }}">
        <div v-if="errors.has('created_by')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_by') }}</div>
    </div>
</div>


