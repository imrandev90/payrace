@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.vehicle.actions.edit', ['name' => $vehicle->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <vehicle-form
                :action="'{{ $vehicle->resource_url }}'"
                :data="{{ $vehicle->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.vehicle.actions.edit', ['name' => $vehicle->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.vehicle.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </vehicle-form>

        </div>
    
</div>

@endsection