@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.organization-staff.actions.edit', ['name' => $organizationStaff->email]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <organization-staff-form
                :action="'{{ $organizationStaff->resource_url }}'"
                :data="{{ $organizationStaff->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.organization-staff.actions.edit', ['name' => $organizationStaff->email]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.organization-staff.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </organization-staff-form>

        </div>
    
</div>

@endsection