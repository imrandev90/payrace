<div class="form-group row align-items-center" :class="{'has-danger': errors.has('complaint_id'), 'has-success': fields.complaint_id && fields.complaint_id.valid }">
    <label for="complaint_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.complaint-detail.columns.complaint_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.complaint_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('complaint_id'), 'form-control-success': fields.complaint_id && fields.complaint_id.valid}" id="complaint_id" name="complaint_id" placeholder="{{ trans('admin.complaint-detail.columns.complaint_id') }}">
        <div v-if="errors.has('complaint_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('complaint_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('vehicle_id'), 'has-success': fields.vehicle_id && fields.vehicle_id.valid }">
    <label for="vehicle_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.complaint-detail.columns.vehicle_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.vehicle_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vehicle_id'), 'form-control-success': fields.vehicle_id && fields.vehicle_id.valid}" id="vehicle_id" name="vehicle_id" placeholder="{{ trans('admin.complaint-detail.columns.vehicle_id') }}">
        <div v-if="errors.has('vehicle_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('route_id'), 'has-success': fields.route_id && fields.route_id.valid }">
    <label for="route_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.complaint-detail.columns.route_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.route_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('route_id'), 'form-control-success': fields.route_id && fields.route_id.valid}" id="route_id" name="route_id" placeholder="{{ trans('admin.complaint-detail.columns.route_id') }}">
        <div v-if="errors.has('route_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('route_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('conductor_id'), 'has-success': fields.conductor_id && fields.conductor_id.valid }">
    <label for="conductor_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.complaint-detail.columns.conductor_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.conductor_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('conductor_id'), 'form-control-success': fields.conductor_id && fields.conductor_id.valid}" id="conductor_id" name="conductor_id" placeholder="{{ trans('admin.complaint-detail.columns.conductor_id') }}">
        <div v-if="errors.has('conductor_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('conductor_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('driver_id'), 'has-success': fields.driver_id && fields.driver_id.valid }">
    <label for="driver_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.complaint-detail.columns.driver_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.driver_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('driver_id'), 'form-control-success': fields.driver_id && fields.driver_id.valid}" id="driver_id" name="driver_id" placeholder="{{ trans('admin.complaint-detail.columns.driver_id') }}">
        <div v-if="errors.has('driver_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('driver_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('organization_id'), 'has-success': fields.organization_id && fields.organization_id.valid }">
    <label for="organization_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.complaint-detail.columns.organization_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.organization_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('organization_id'), 'form-control-success': fields.organization_id && fields.organization_id.valid}" id="organization_id" name="organization_id" placeholder="{{ trans('admin.complaint-detail.columns.organization_id') }}">
        <div v-if="errors.has('organization_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('organization_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('inspector_id'), 'has-success': fields.inspector_id && fields.inspector_id.valid }">
    <label for="inspector_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.complaint-detail.columns.inspector_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.inspector_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('inspector_id'), 'form-control-success': fields.inspector_id && fields.inspector_id.valid}" id="inspector_id" name="inspector_id" placeholder="{{ trans('admin.complaint-detail.columns.inspector_id') }}">
        <div v-if="errors.has('inspector_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('inspector_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('trip_id'), 'has-success': fields.trip_id && fields.trip_id.valid }">
    <label for="trip_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.complaint-detail.columns.trip_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.trip_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('trip_id'), 'form-control-success': fields.trip_id && fields.trip_id.valid}" id="trip_id" name="trip_id" placeholder="{{ trans('admin.complaint-detail.columns.trip_id') }}">
        <div v-if="errors.has('trip_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('trip_id') }}</div>
    </div>
</div>


