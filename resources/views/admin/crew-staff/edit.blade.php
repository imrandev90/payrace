@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.crew-staff.actions.edit', ['name' => $crewStaff->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <crew-staff-form
                :action="'{{ $crewStaff->resource_url }}'"
                :data="{{ $crewStaff->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.crew-staff.actions.edit', ['name' => $crewStaff->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.crew-staff.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </crew-staff-form>

        </div>
    
</div>

@endsection