@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.crew-staff.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <crew-staff-form
            :action="'{{ url('admin/crew-staffs') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.crew-staff.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.crew-staff.components.form-elements')
                </div>
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </crew-staff-form>

        </div>

        </div>

    
@endsection