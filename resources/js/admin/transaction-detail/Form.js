import AppForm from '../app-components/Form/AppForm';

Vue.component('transaction-detail-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                payment_id:  '' ,
                vehicle_id:  '' ,
                route_id:  '' ,
                conductor_id:  '' ,
                driver_id:  '' ,
                organization_id:  '' ,
                inspector_id:  '' ,
                trip_id:  '' ,
                
            }
        }
    }

});