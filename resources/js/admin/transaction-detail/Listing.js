import AppListing from '../app-components/Listing/AppListing';

Vue.component('transaction-detail-listing', {
    mixins: [AppListing]
});