import AppForm from '../app-components/Form/AppForm';

Vue.component('transaction-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                payee_id:  '' ,
                phone:  '' ,
                full_name:  '' ,
                account:  '' ,
                paybill:  '' ,
                amount:  '' ,
                channel:  '' ,
                payment_details:  '' ,
                time:  '' ,
                vehicle_id:  '' ,
                route_id:  '' ,
                conductor_id:  '' ,
                driver_id:  '' ,
                organization_id:  '' ,
                inspector_id:  '' ,
                trip_id:  '' ,
                
            }
        }
    }

});