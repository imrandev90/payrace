Vue.component('reports-view', {
	data: function() {
        return {
            config: {
              dateFormat: 'Y-m-d H:i:s',
              enableTime: true,
            },    

            todayString: new Date().toDateString(),
            startDate: new Date().toIsoString() + " 00:00:01",
            endDate: new Date().toIsoString() + " 23:59:59",

            selected_control: { label: 'All', value: 'All' },
            controls: [
                { label: 'All', value: 'ALL' },
                { label: 'Today', value: 'TODAY' },
                { label: 'Yesterday', value: 'YESTERDAY' },
                { label: 'Last 7 days', value: 'LAST7DAYS' },
                { label: 'Other', value: 'OTHER' },
            ],

            showFilter:false,
        }
    },


    watch: {
        // due_today: {
        //     immediate: true, 
        //     handler (val, oldVal) {

        //     }
        // },
    }, 



    mounted(){
        const queryString = window.location.search;

        if (queryString.length > 0) {
            const urlParams = new URLSearchParams(queryString);
            const start = urlParams.get('start')
            const end = urlParams.get('end')

            const startDate = moment(start);
            const endDate = moment(end);
                
            if (startDate.format("YYYY-MM-DD") === endDate.format("YYYY-MM-DD")) {
                if (moment().isSame(endDate, 'days')) {
                    this.selected_control =  { label: 'Today', value: 'TODAY' };
                }

                if (moment().diff(endDate, 'days') == 1) {
                    this.selected_control =  { label: 'Yesterday', value: 'YESTERDAY' };
                }
            } else{

                if (!moment().isSame(endDate, 'days') && startDate.diff(endDate, 'days') == 7) {
                    this.selected_control =  { label: 'Last 7 days', value: 'LAST7DAYS' };
                } else{
                    this.selected_control =  { label: 'Other', value: 'OTHER' };
                }

                if(this.selected_control.value == "OTHER"){
                    this.showFilter = true;
                } else{
                    this.showFilter = false;
                }
            } 


            if(!start && !end){
                this.selected_control =  { label: 'All', value: 'ALL' };
            }
        }


        
    },


    methods:{
        updateDashboard(){

        },

        onSelect(optionsSelected) {
            if (optionsSelected.length > 0) {
                this.selected_control = optionsSelected[0]

                if(this.selected_control.value == "ALL"){
                   const queryString = window.location.search;
                    const urlParams = new URLSearchParams(queryString);
                    const id = urlParams.get('id')

                    var url = window.location.href.split('?')[0];

                    url += '?id=' + id;
                    
                    window.location.href = url;

                }

                if(this.selected_control.value == "OTHER"){
                    this.showFilter = true;
                } else{
                    this.showFilter = false;
                }

                if(this.selected_control.value == "TODAY"){
                    this.startDate = moment().format('YYYY-MM-DD');
                    this.endDate = moment().format('YYYY-MM-DD');
                    this.updateReport();
                }

                if(this.selected_control.value == "YESTERDAY"){
                    this.startDate = moment().subtract(1,'d').format('YYYY-MM-DD');
                    this.endDate = moment().subtract(1,'d').format('YYYY-MM-DD');
                    this.updateReport();
                }

                if(this.selected_control.value == "LAST7DAYS"){
                    this.startDate = moment().subtract(7,'d').format('YYYY-MM-DD');
                    this.endDate = moment().format('YYYY-MM-DD');
                    this.updateReport();
                }
            }
        },

        updateReport(){
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            const id = urlParams.get('id')
            
            var url = window.location.href.split('?')[0];

            url += '?id=' + id;

            if(this.startDate){
                url += "&start=" + this.startDate;
            } 

            if(this.endDate){
                url += "&end=" + this.endDate;
            } 

            window.location.href = url;
        },
    },


    props: {
        // 'due_today': {
        //     type: Array,
        // },

        'selected_vehicle': {
            type: Object,
        },

        'selected_owner': {
            type: Object,
        },

        'selected_route': {
            type: Object,
        },

        'selected_driver': {
            type: Object,
        },

        'selected_conductor': {
            type: Object,
        },
    },
});