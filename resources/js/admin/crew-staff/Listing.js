import AppListing from '../app-components/Listing/AppListing';

Vue.component('crew-staff-listing', {
    mixins: [AppListing]
});