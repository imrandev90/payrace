import AppForm from '../app-components/Form/AppForm';

Vue.component('crew-staff-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                full_name:  '' ,
                phone:  '' ,
                address:  '' ,
                id_number:  '' ,
                designation:  '' ,
                description:  '' ,
                organization_id:  '' ,  
            },
            
            selected_organization:'',
        }
    },

    watch: {
        'organization': function (newSelectedType) {
            this.form.organization_id = newSelectedType.id
            this.selected_organization = newSelectedType
        },

        'selected_organization': function (newSelectedType) {
            this.form.organization_id = newSelectedType.id
        },
    }, 

    props: {
        'organization': {
            type: Object,
        },

        'organizations': {
            type: Array,
        },
    }

});