import AppListing from '../app-components/Listing/AppListing';

Vue.component('owner-listing', {
    mixins: [AppListing]
});