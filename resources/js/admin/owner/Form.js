import AppForm from '../app-components/Form/AppForm';

Vue.component('owner-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                full_name:  '' ,
                phone:  '' ,
                email:  '' ,
                address:  '' ,
                
            }
        }
    }

});