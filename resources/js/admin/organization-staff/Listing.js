import AppListing from '../app-components/Listing/AppListing';

Vue.component('organization-staff-listing', {
    mixins: [AppListing]
});