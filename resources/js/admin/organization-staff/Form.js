import AppForm from '../app-components/Form/AppForm';

Vue.component('organization-staff-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                full_name:  '' ,
                email:  '' ,
                phone:  '' ,
                address:  '' ,
                designation:  '' ,
                organization_id:  '' ,
                password:  '' ,
                activated:  false ,
                forbidden:  false ,
                language:  '' ,
                
            },
            
            selected_organization:'',
        }
    },

    watch: {
        'organization': function (newSelectedType) {
            this.form.organization_id = newSelectedType.id
            this.selected_organization = newSelectedType
        },

        'selected_organization': function (newSelectedType) {
            this.form.organization_id = newSelectedType.id
        },
    }, 

    props: {
        'organization': {
            type: Object,
        },

        'organizations': {
            type: Array,
        },
    }

});