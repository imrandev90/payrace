import AppForm from '../app-components/Form/AppForm';

Vue.component('complaint-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                phone:  '' ,
                title:  '' ,
                description:  '' ,
                handled_by:  '' ,
                handled_at:  '' ,
                
            }
        }
    }

});