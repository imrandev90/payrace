import AppListing from '../app-components/Listing/AppListing';

Vue.component('complaint-listing', {
    mixins: [AppListing]
});