import AppForm from '../app-components/Form/AppForm';

Vue.component('organization-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title:  '' ,
                address:  '' ,
                contact_name:  '' ,
                contact_phone:  '' ,
            }
        }
    },

});