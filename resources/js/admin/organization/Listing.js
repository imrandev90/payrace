import AppListing from '../app-components/Listing/AppListing';

Vue.component('organization-listing', {
    mixins: [AppListing]
});