import AppListing from '../app-components/Listing/AppListing';

Vue.component('complaint-detail-listing', {
    mixins: [AppListing]
});