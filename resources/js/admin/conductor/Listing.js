import AppListing from '../app-components/Listing/AppListing';

Vue.component('conductor-listing', {
    mixins: [AppListing]
});