import moment from 'moment'

Date.prototype.toIsoString = function() {
        var tzo = -this.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',

        pad = function(num) {
            var norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };



    return this.getFullYear() +
        '-' + pad(this.getMonth() + 1) +
        '-' + pad(this.getDate());
}


Vue.filter('TimeAgo', function(value) {
  if (value) {
    return moment(value).fromNow();
  }
});


Vue.component('dashboard-view', {
	data: function() {
        return {
            config: {
              dateFormat: 'Y-m-d H:i:s',
              enableTime: true,
            },    

            todayString: new Date().toDateString(),
            startDate: new Date().toIsoString() + " 00:00:01",
            endDate: new Date().toIsoString() + " 23:59:59",

            pieData: null,
        }
    },


    watch: {
       
    }, 


    mounted(){
        this.updateDashboard()

    },


    methods:{
        updateDashboard(){
            this.updatePieChart()
        },

        updatePieChart(){
            this.pieData = {
              chart: {
                type: "pie",
                options3d: {
                  enabled: true,
                  alpha: 45
                }
              },

              title: {
                text: "Earning by Route: Today"
              },

              subtitle: {
                text: ""
              },

              plotOptions: {
                pie: {
                  innerSize: 100,
                  depth: 45
                }
              },

              series: [{
                  name: "Amount Earned",

                  data: [
                    ["Route 1", 8],
                    ["Route 2", 3],
                    ["Route 3", 1],
                    ["Route 4", 6],
                    ["Route 5", 8],
                    ["Route 6", 4],
                    ["Route 7", 4],
                    ["Route 8", 1],
                    ["Route 9", 1]
                  ]
                }]
            };

            // this.renderChart(this.pieData);
        },
    },


    props: {

    },
});