import AppListing from '../app-components/Listing/AppListing';

Vue.component('inspector-listing', {
    mixins: [AppListing]
});