import AppForm from '../app-components/Form/AppForm';

Vue.component('inspector-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                full_name:  '' ,
                phone:  '' ,
                address:  '' ,
                id_number:  '' ,
                organization_id:  '' ,
                
            }
        }
    }

});