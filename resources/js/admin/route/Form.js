import AppForm from '../app-components/Form/AppForm';

Vue.component('route-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title:  '' ,
                number:  '' ,
                description:  '' ,
                distance:  '' ,
                
            }
        }
    }

});