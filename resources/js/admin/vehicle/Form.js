import AppForm from '../app-components/Form/AppForm';

Vue.component('vehicle-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                registration_id:  '' ,
                fleet_number:  '' ,
                payment_account:  '' ,
                manu_date:  '' ,
                owner_id:  '' ,
                conductor_id:  '' ,
                driver_id:  '' ,
                inspector_id:  '' ,
                created_by:  '' ,
            },

            selected_owner:'',
            selected_conductor:'',
            selected_driver:'',
            selected_inspector:'',
        }
    },

     watch: {
        'owner': function (newSelectedType) {
            this.form.owner_id = newSelectedType.id
            this.selected_owner = newSelectedType
        },

        'selected_owner': function (newSelectedType) {
            this.form.owner_id = newSelectedType.id
        },


        'conductor': function (newSelectedType) {
            this.form.conductor_id = newSelectedType.id
            this.selected_conductor = newSelectedType
        },

        'selected_conductor': function (newSelectedType) {
            this.form.conductor_id = newSelectedType.id
        },


        'driver': function (newSelectedType) {
            this.form.driver_id = newSelectedType.id
            this.selected_driver = newSelectedType
        },

        'selected_driver': function (newSelectedType) {
            this.form.driver_id = newSelectedType.id
        },


        'inspector': function (newSelectedType) {
            this.form.inspector_id = newSelectedType.id
            this.selected_inspector = newSelectedType
        },

        'selected_inspector': function (newSelectedType) {
            this.form.inspector_id = newSelectedType.id
        },
    }, 

    props: {
        'owner': {
            type: Object,
        },

        'owners': {
            type: Array,
        },


        'conductor': {
            type: Object,
        },

        'conductors': {
            type: Array,
        },


        'driver': {
            type: Object,
        },

        'drivers': {
            type: Array,
        },


        'inspector': {
            type: Object,
        },

        'inspectors': {
            type: Array,
        },
    }

});