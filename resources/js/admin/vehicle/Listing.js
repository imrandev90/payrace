import AppListing from '../app-components/Listing/AppListing';

Vue.component('vehicle-listing', {
    mixins: [AppListing]
});