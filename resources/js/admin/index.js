import SegmentedControl from './segmented/SegmentedControl.vue'
Vue.use(SegmentedControl);
Vue.component('vue-segmented-control', SegmentedControl);


import './admin-user';
import './profile-edit-profile';
import './profile-edit-password';
import './organization';
import './organization-staff';
import './owner';
import './route';
import './crew-staff';
import './vehicle';
import './transaction';
import './transaction-detail';
import './complaint';
import './complaint-detail';

import './dashboard';
import './reports';
import './driver';
import './conductor';
import './inspector';

