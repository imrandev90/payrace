<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer("payee_id")->nullable();
            $table->string("phone")->nullable();
            $table->string("full_name")->nullble();
            $table->string("account")->nullable();
            $table->string("paybill")->nullable();
            $table->string("amount");
            $table->string("channel")->default("CASH");
            $table->text("payment_details")->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
