<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string("registration_id");
            $table->string("fleet_number");
            $table->string("payment_account");
            $table->date("manu_date")->nullable();

            $table->integer("owner_id")->nullable();
            $table->integer("conductor_id")->nullable();
            $table->integer("driver_id")->nullable();
            $table->integer("inspector_id")->nullable();
            $table->integer("created_by")->nullable();

            $table->timestamps();
        });
    }

 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
