<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->integer("payment_id")->nullable();
            $table->integer("vehicle_id")->nullable();
            $table->integer("route_id")->nullable();
            $table->integer("conductor_id")->nullable();
            $table->integer("driver_id")->nullable();
            $table->integer("organization_id")->nullable();
            $table->integer("inspector_id")->nullable();
            $table->integer("trip_id")->nullable();
            $table->timestamps();
        });
    }

   


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
