<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('transactions', function (Blueprint $table) {
            $table->integer("vehicle_id")->nullable();
            $table->integer("route_id")->nullable();
            $table->integer("conductor_id")->nullable();
            $table->integer("driver_id")->nullable();
            $table->integer("organization_id")->nullable();
            $table->integer("inspector_id")->nullable();
            $table->integer("trip_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('vehicle_id');
            $table->dropColumn('route_id');
            $table->dropColumn('conductor_id');
            $table->dropColumn('driver_id');
            $table->dropColumn('organization_id');
            $table->dropColumn('inspector_id');
            $table->dropColumn('trip_id');
        });
    }
}
