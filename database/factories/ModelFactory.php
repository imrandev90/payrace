<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'activated' => true,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
    ];
});/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Organization::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'address' => $faker->sentence,
        'contact_name' => $faker->sentence,
        'contact_phone' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\OrganizationStaff::class, static function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->sentence,
        'email' => $faker->email,
        'phone' => $faker->sentence,
        'address' => $faker->sentence,
        'designation' => $faker->sentence,
        'organization_id' => $faker->randomNumber(5),
        'password' => bcrypt($faker->password),
        'activated' => $faker->boolean(),
        'forbidden' => $faker->boolean(),
        'language' => $faker->sentence,
        'remember_token' => null,
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Owner::class, static function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->sentence,
        'phone' => $faker->sentence,
        'email' => $faker->email,
        'address' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Route::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'number' => $faker->sentence,
        'description' => $faker->text(),
        'distance' => $faker->randomFloat,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\CrewStaff::class, static function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->sentence,
        'phone' => $faker->sentence,
        'address' => $faker->sentence,
        'id_number' => $faker->sentence,
        'designation' => $faker->sentence,
        'description' => $faker->text(),
        'organization_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Vehicle::class, static function (Faker\Generator $faker) {
    return [
        'registration_id' => $faker->sentence,
        'fleet_number' => $faker->sentence,
        'payment_account' => $faker->sentence,
        'manu_date' => $faker->date(),
        'owner_id' => $faker->randomNumber(5),
        'conductor_id' => $faker->randomNumber(5),
        'driver_id' => $faker->randomNumber(5),
        'inspector_id' => $faker->randomNumber(5),
        'created_by' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Transaction::class, static function (Faker\Generator $faker) {
    return [
        'payee_id' => $faker->randomNumber(5),
        'phone' => $faker->sentence,
        'full_name' => $faker->sentence,
        'account' => $faker->sentence,
        'paybill' => $faker->sentence,
        'amount' => $faker->sentence,
        'channel' => $faker->sentence,
        'payment_details' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\TransactionDetail::class, static function (Faker\Generator $faker) {
    return [
        'payment_id' => $faker->randomNumber(5),
        'vehicle_id' => $faker->randomNumber(5),
        'route_id' => $faker->randomNumber(5),
        'conductor_id' => $faker->randomNumber(5),
        'driver_id' => $faker->randomNumber(5),
        'organization_id' => $faker->randomNumber(5),
        'inspector_id' => $faker->randomNumber(5),
        'trip_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Complaint::class, static function (Faker\Generator $faker) {
    return [
        'phone' => $faker->sentence,
        'title' => $faker->sentence,
        'description' => $faker->text(),
        'handled_by' => $faker->randomNumber(5),
        'handled_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ComplaintDetail::class, static function (Faker\Generator $faker) {
    return [
        'complaint_id' => $faker->randomNumber(5),
        'vehicle_id' => $faker->randomNumber(5),
        'route_id' => $faker->randomNumber(5),
        'conductor_id' => $faker->randomNumber(5),
        'driver_id' => $faker->randomNumber(5),
        'organization_id' => $faker->randomNumber(5),
        'inspector_id' => $faker->randomNumber(5),
        'trip_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Driver::class, static function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->sentence,
        'phone' => $faker->sentence,
        'address' => $faker->sentence,
        'id_number' => $faker->sentence,
        'organization_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Conductor::class, static function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->sentence,
        'phone' => $faker->sentence,
        'address' => $faker->sentence,
        'id_number' => $faker->sentence,
        'organization_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Inspector::class, static function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->sentence,
        'phone' => $faker->sentence,
        'address' => $faker->sentence,
        'id_number' => $faker->sentence,
        'organization_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Transaction::class, static function (Faker\Generator $faker) {
    return [
        'payee_id' => $faker->randomNumber(5),
        'phone' => $faker->sentence,
        'full_name' => $faker->sentence,
        'account' => $faker->sentence,
        'paybill' => $faker->sentence,
        'amount' => $faker->randomNumber(5),
        'channel' => $faker->sentence,
        'payment_details' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'time' => $faker->dateTime,
        'vehicle_id' => $faker->randomNumber(5),
        'route_id' => $faker->randomNumber(5),
        'conductor_id' => $faker->randomNumber(5),
        'driver_id' => $faker->randomNumber(5),
        'organization_id' => $faker->randomNumber(5),
        'inspector_id' => $faker->randomNumber(5),
        'trip_id' => $faker->randomNumber(5),
        
        
    ];
});
