<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TransactionDetail;

class ImportTransactionDetails extends Command{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:transaction_details';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import transactions from stored csv files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        //set the path for the csv files
        $path = base_path("storage/app/public/transaction-details/*.csv"); 
        
        //run 2 loops at a time 
        foreach (array_slice(glob($path),0,5) as $file) {
            
            //read the data into an array
            $data = array_map('str_getcsv', file($file));

            //loop over the data
            foreach($data as $row) {

                TransactionDetail::create([
                    'id'     => $row[0],
                    'payment_id'       => $row[1], 
                    'vehicle_id'   => $row[2],
                    'route_id'     => $row[3],

                    // 'conductor_id'      => $row[5],
                    // 'driver_id'      => $row[6],
                    // 'inspector_id'      => $row[7],

                    'conductor_id'      => $row[5],
                    'organization_id'      => $row[6],
                    'driver_id'      => null,
                    'inspector_id'      => null,
                    'trip_id'      => null,
                ]);

            }

            //delete the file
            unlink($file);
        }

        // return 0;
    }
}
