<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Transaction;

class ImportTransactions extends Command{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import transactions from stored csv files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        //set the path for the csv files
        $path = base_path("storage/app/public/transactions/*.csv"); 



        //run 2 loops at a time 
        foreach (array_slice(glob($path), 0, 5) as $file) {
            

            //read the data into an array
            $data = array_map('str_getcsv', file($file));

            //loop over the data
            foreach($data as $row) {

                $phone = "0".$row[1];
                $full_name = str_replace('\'', "", $row[3]);
                $account = str_replace('\'', "", $row[4]);
                $channel = str_replace('\'', "", $row[7]);

                $transaction = Transaction::create([
                    'id'     => $row[0],
                    'payee_id'       => $row[1], 
                    'phone'   => $phone,
                    'full_name'     => $full_name,
                    'account'      => $account,
                    'paybill'      => $row[5],
                    'amount'      => $row[6],
                    'channel'      => $channel,
                    'payment_details'      => $row[8],


                    'time' => \Carbon\Carbon::parse(str_replace('/', '-', $row[9])),
                ]);


                // \Log::debug($transaction->id." successfully imported");
            }

            //delete the file
            unlink($file);
        }

        // return 0;
    }
}
