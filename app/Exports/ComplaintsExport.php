<?php

namespace App\Exports;

use App\Models\Complaint;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ComplaintsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Complaint::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.complaint.columns.id'),
            trans('admin.complaint.columns.phone'),
            trans('admin.complaint.columns.title'),
            trans('admin.complaint.columns.description'),
            trans('admin.complaint.columns.handled_by'),
            trans('admin.complaint.columns.handled_at'),
        ];
    }

    /**
     * @param Complaint $complaint
     * @return array
     *
     */
    public function map($complaint): array
    {
        return [
            $complaint->id,
            $complaint->phone,
            $complaint->title,
            $complaint->description,
            $complaint->handled_by,
            $complaint->handled_at,
        ];
    }
}
