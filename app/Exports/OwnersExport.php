<?php

namespace App\Exports;

use App\Models\Owner;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OwnersExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Owner::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.owner.columns.id'),
            trans('admin.owner.columns.full_name'),
            trans('admin.owner.columns.phone'),
            trans('admin.owner.columns.email'),
            trans('admin.owner.columns.address'),
        ];
    }

    /**
     * @param Owner $owner
     * @return array
     *
     */
    public function map($owner): array
    {
        return [
            $owner->id,
            $owner->full_name,
            $owner->phone,
            $owner->email,
            $owner->address,
        ];
    }
}
