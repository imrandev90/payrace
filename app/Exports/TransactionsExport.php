<?php

namespace App\Exports;

use App\Models\Transaction;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransactionsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Transaction::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.transaction.columns.id'),
            trans('admin.transaction.columns.payee_id'),
            trans('admin.transaction.columns.phone'),
            trans('admin.transaction.columns.full_name'),
            trans('admin.transaction.columns.account'),
            trans('admin.transaction.columns.paybill'),
            trans('admin.transaction.columns.amount'),
            trans('admin.transaction.columns.channel'),
            trans('admin.transaction.columns.payment_details'),
            trans('admin.transaction.columns.time'),
            trans('admin.transaction.columns.vehicle_id'),
            trans('admin.transaction.columns.route_id'),
            trans('admin.transaction.columns.conductor_id'),
            trans('admin.transaction.columns.driver_id'),
            trans('admin.transaction.columns.organization_id'),
            trans('admin.transaction.columns.inspector_id'),
            trans('admin.transaction.columns.trip_id'),
        ];
    }

    /**
     * @param Transaction $transaction
     * @return array
     *
     */
    public function map($transaction): array
    {
        return [
            $transaction->id,
            $transaction->payee_id,
            $transaction->phone,
            $transaction->full_name,
            $transaction->account,
            $transaction->paybill,
            $transaction->amount,
            $transaction->channel,
            $transaction->payment_details,
            $transaction->time,
            $transaction->vehicle_id,
            $transaction->route_id,
            $transaction->conductor_id,
            $transaction->driver_id,
            $transaction->organization_id,
            $transaction->inspector_id,
            $transaction->trip_id,
        ];
    }
}
