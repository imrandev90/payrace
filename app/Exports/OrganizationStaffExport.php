<?php

namespace App\Exports;

use App\Models\OrganizationStaff;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OrganizationStaffExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return OrganizationStaff::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.organization-staff.columns.id'),
            trans('admin.organization-staff.columns.full_name'),
            trans('admin.organization-staff.columns.email'),
            trans('admin.organization-staff.columns.phone'),
            trans('admin.organization-staff.columns.address'),
            trans('admin.organization-staff.columns.designation'),
            trans('admin.organization-staff.columns.organization_id'),
            trans('admin.organization-staff.columns.activated'),
            trans('admin.organization-staff.columns.forbidden'),
            trans('admin.organization-staff.columns.language'),
        ];
    }

    /**
     * @param OrganizationStaff $organizationStaff
     * @return array
     *
     */
    public function map($organizationStaff): array
    {
        return [
            $organizationStaff->id,
            $organizationStaff->full_name,
            $organizationStaff->email,
            $organizationStaff->phone,
            $organizationStaff->address,
            $organizationStaff->designation,
            $organizationStaff->organization_id,
            $organizationStaff->activated,
            $organizationStaff->forbidden,
            $organizationStaff->language,
        ];
    }
}
