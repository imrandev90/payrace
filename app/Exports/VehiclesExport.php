<?php

namespace App\Exports;

use App\Models\Vehicle;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class VehiclesExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Vehicle::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.vehicle.columns.id'),
            trans('admin.vehicle.columns.registration_id'),
            trans('admin.vehicle.columns.fleet_number'),
            trans('admin.vehicle.columns.payment_account'),
            trans('admin.vehicle.columns.manu_date'),
            trans('admin.vehicle.columns.owner_id'),
            trans('admin.vehicle.columns.conductor_id'),
            trans('admin.vehicle.columns.driver_id'),
            trans('admin.vehicle.columns.inspector_id'),
            trans('admin.vehicle.columns.created_by'),
        ];
    }

    /**
     * @param Vehicle $vehicle
     * @return array
     *
     */
    public function map($vehicle): array
    {
        return [
            $vehicle->id,
            $vehicle->registration_id,
            $vehicle->fleet_number,
            $vehicle->payment_account,
            $vehicle->manu_date,
            $vehicle->owner_id,
            $vehicle->conductor_id,
            $vehicle->driver_id,
            $vehicle->inspector_id,
            $vehicle->created_by,
        ];
    }
}
