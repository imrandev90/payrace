<?php

namespace App\Exports;

use App\Models\ComplaintDetail;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ComplaintDetailsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return ComplaintDetail::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.complaint-detail.columns.id'),
            trans('admin.complaint-detail.columns.complaint_id'),
            trans('admin.complaint-detail.columns.vehicle_id'),
            trans('admin.complaint-detail.columns.route_id'),
            trans('admin.complaint-detail.columns.conductor_id'),
            trans('admin.complaint-detail.columns.driver_id'),
            trans('admin.complaint-detail.columns.organization_id'),
            trans('admin.complaint-detail.columns.inspector_id'),
            trans('admin.complaint-detail.columns.trip_id'),
        ];
    }

    /**
     * @param ComplaintDetail $complaintDetail
     * @return array
     *
     */
    public function map($complaintDetail): array
    {
        return [
            $complaintDetail->id,
            $complaintDetail->complaint_id,
            $complaintDetail->vehicle_id,
            $complaintDetail->route_id,
            $complaintDetail->conductor_id,
            $complaintDetail->driver_id,
            $complaintDetail->organization_id,
            $complaintDetail->inspector_id,
            $complaintDetail->trip_id,
        ];
    }
}
