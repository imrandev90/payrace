<?php

namespace App\Exports;

use App\Models\Driver;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DriversExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Driver::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.driver.columns.id'),
            trans('admin.driver.columns.full_name'),
            trans('admin.driver.columns.phone'),
            trans('admin.driver.columns.address'),
            trans('admin.driver.columns.id_number'),
            trans('admin.driver.columns.organization_id'),
        ];
    }

    /**
     * @param Driver $driver
     * @return array
     *
     */
    public function map($driver): array
    {
        return [
            $driver->id,
            $driver->full_name,
            $driver->phone,
            $driver->address,
            $driver->id_number,
            $driver->organization_id,
        ];
    }
}
