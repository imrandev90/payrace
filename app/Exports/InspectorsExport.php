<?php

namespace App\Exports;

use App\Models\Inspector;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class InspectorsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Inspector::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.inspector.columns.id'),
            trans('admin.inspector.columns.full_name'),
            trans('admin.inspector.columns.phone'),
            trans('admin.inspector.columns.address'),
            trans('admin.inspector.columns.id_number'),
            trans('admin.inspector.columns.organization_id'),
        ];
    }

    /**
     * @param Inspector $inspector
     * @return array
     *
     */
    public function map($inspector): array
    {
        return [
            $inspector->id,
            $inspector->full_name,
            $inspector->phone,
            $inspector->address,
            $inspector->id_number,
            $inspector->organization_id,
        ];
    }
}
