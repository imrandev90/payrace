<?php

namespace App\Exports;

use App\Models\Conductor;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ConductorsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Conductor::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.conductor.columns.id'),
            trans('admin.conductor.columns.full_name'),
            trans('admin.conductor.columns.phone'),
            trans('admin.conductor.columns.address'),
            trans('admin.conductor.columns.id_number'),
            trans('admin.conductor.columns.organization_id'),
        ];
    }

    /**
     * @param Conductor $conductor
     * @return array
     *
     */
    public function map($conductor): array
    {
        return [
            $conductor->id,
            $conductor->full_name,
            $conductor->phone,
            $conductor->address,
            $conductor->id_number,
            $conductor->organization_id,
        ];
    }
}
