<?php

namespace App\Exports;

use App\Models\Organization;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OrganizationsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Organization::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.organization.columns.id'),
            trans('admin.organization.columns.title'),
            trans('admin.organization.columns.address'),
            trans('admin.organization.columns.contact_name'),
            trans('admin.organization.columns.contact_phone'),
        ];
    }

    /**
     * @param Organization $organization
     * @return array
     *
     */
    public function map($organization): array
    {
        return [
            $organization->id,
            $organization->title,
            $organization->address,
            $organization->contact_name,
            $organization->contact_phone,
        ];
    }
}
