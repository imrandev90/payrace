<?php

namespace App\Exports;

use App\Models\CrewStaff;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CrewStaffExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return CrewStaff::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.crew-staff.columns.id'),
            trans('admin.crew-staff.columns.full_name'),
            trans('admin.crew-staff.columns.phone'),
            trans('admin.crew-staff.columns.address'),
            trans('admin.crew-staff.columns.id_number'),
            trans('admin.crew-staff.columns.designation'),
            trans('admin.crew-staff.columns.description'),
            trans('admin.crew-staff.columns.organization_id'),
        ];
    }

    /**
     * @param CrewStaff $crewStaff
     * @return array
     *
     */
    public function map($crewStaff): array
    {
        return [
            $crewStaff->id,
            $crewStaff->full_name,
            $crewStaff->phone,
            $crewStaff->address,
            $crewStaff->id_number,
            $crewStaff->designation,
            $crewStaff->description,
            $crewStaff->organization_id,
        ];
    }
}
