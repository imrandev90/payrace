<?php

namespace App\Exports;

use App\Models\Route;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RoutesExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Route::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.route.columns.id'),
            trans('admin.route.columns.title'),
            trans('admin.route.columns.number'),
            trans('admin.route.columns.description'),
            trans('admin.route.columns.distance'),
        ];
    }

    /**
     * @param Route $route
     * @return array
     *
     */
    public function map($route): array
    {
        return [
            $route->id,
            $route->title,
            $route->number,
            $route->description,
            $route->distance,
        ];
    }
}
