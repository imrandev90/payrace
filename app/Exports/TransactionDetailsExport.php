<?php

namespace App\Exports;

use App\Models\TransactionDetail;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransactionDetailsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return TransactionDetail::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('admin.transaction-detail.columns.id'),
            trans('admin.transaction-detail.columns.payment_id'),
            trans('admin.transaction-detail.columns.vehicle_id'),
            trans('admin.transaction-detail.columns.route_id'),
            trans('admin.transaction-detail.columns.conductor_id'),
            trans('admin.transaction-detail.columns.driver_id'),
            trans('admin.transaction-detail.columns.organization_id'),
            trans('admin.transaction-detail.columns.inspector_id'),
            trans('admin.transaction-detail.columns.trip_id'),
        ];
    }

    /**
     * @param TransactionDetail $transactionDetail
     * @return array
     *
     */
    public function map($transactionDetail): array
    {
        return [
            $transactionDetail->id,
            $transactionDetail->payment_id,
            $transactionDetail->vehicle_id,
            $transactionDetail->route_id,
            $transactionDetail->conductor_id,
            $transactionDetail->driver_id,
            $transactionDetail->organization_id,
            $transactionDetail->inspector_id,
            $transactionDetail->trip_id,
        ];
    }
}
