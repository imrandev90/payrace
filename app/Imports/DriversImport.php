<?php
namespace App\Imports;

use App\Models\Driver;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class DriversImport implements ToModel{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */


    public function model(array $row){        
        return new Driver([
            'id'     => $row[0],
            'full_name'       => $row[1], 
            'phone'       => $row[2], 
            'address'   => $row[3],
            'id_number'     => $row[4],
            'organization_id'      => $row[5],
        ]);
    }
}