<?php
namespace App\Imports;

use App\Models\Inspector;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class InspectorsImport implements ToModel{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row){
        return new Inspector([
            'id'     => $row[0],
            'full_name'       => $row[1], 
            'phone'       => $row[2], 
            'address'   => $row[3],
            'id_number'     => $row[4],
            'organization_id'      => $row[5],
        ]);
    }
}