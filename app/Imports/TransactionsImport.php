<?php

namespace App\Imports;

use App\Models\Transaction;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class TransactionsImport implements ToModel{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

// 1, 1, 740307601, 'NO NAME',  '144', 111719, 50, 'mpesa', 03/08/20 05:00:00 NO_NAME PAID KES 50.00 to  '144', 03/08/20 05:00:00



    public function model(array $row){

        $phone = "0"+$row[1];
        $full_name = str_replace('\'', "", $row[3]);
        $account = str_replace('\'', "", $row[4]);
        $channel = str_replace('\'', "", $row[7]);

        return new Transaction([
            'id'     => $row[0],
            'payee_id'       => $row[1], 
            'phone'   => $phone,
            'full_name'     => $full_name,
            'account'      => $account,
            'paybill'      => $row[5],
            'amount'      => $row[6],
            'channel'      => $channel,
            'payment_details'      => $row[8],
            'created_at'      => \Carbon\Carbon::parse($row[9]),
        ]);
    }
}