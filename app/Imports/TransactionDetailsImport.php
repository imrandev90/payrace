<?php

namespace App\Imports;

use App\Models\TransactionDetail;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class TransactionDetailsImport implements ToModel{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row){
        return new TransactionDetail([
            'id'     => $row[0],
            'payment_id'       => $row[1], 
            'vehicle_id'   => $row[2],
            'route_id'     => $row[3],
            'conductor_id'      => $row[5],
            'driver_id'      => $row[6],
            'inspector_id'      => $row[7],
            'trip_id'      => null,
        ]);
    }
}