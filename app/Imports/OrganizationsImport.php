<?php
namespace App\Imports;

use App\Models\Organization;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class OrganizationsImport implements ToModel{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row){        
        return new Organization([
            'id'     => $row[0],
            'title'       => $row[1], 
            'address'   => $row[2],
            'contact_name'     => $row[3],
            'contact_phone'      => $row[4],
        ]);
    }
}