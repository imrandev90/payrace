<?php

namespace App\Imports;

use App\Models\Owner;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class OwnersImport implements ToModel{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */ 

    public function model(array $row){
        return new Owner([
            'id'     => $row[0],
            'full_name'       => $row[1], 
            'phone'   => "0".$row[2],
            'email'     => $row[3],
            'address'      => $row[4],
        ]);
    }
}