<?php

namespace App\Imports;

use App\Models\Route;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class RoutesImport implements ToModel{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */ 
    public function model(array $row){
        return new Route([
            'id'     => $row[0],
            'title'       => $row[1], 
            'number'   => $row[2],
            'description'     => $row[3],
            'distance'      => $row[4],
        ]);
    }
}