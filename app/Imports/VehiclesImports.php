<?php

namespace App\Imports;

use App\Models\Vehicle;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class VehiclesImport implements ToModel{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row){
        return new Vehicle([
            'id'     => $row[0],
            'registration_id'       => $row[1], 
            'fleet_number'   => $row[2],
            'payment_account'     => $row[3],
            'manu_date'      => date('Y-m-d', strtotime($row[4])),

            // 'organization_id'      => $row[5],

            'total_seats'      => $row[6],
            'filled_seats'      => $row[7],

            'owner_id'      => $row[8],

            'conductor_id'      => null,
            'driver_id'      => null,
            'inspector_id'      => null,

            'created_by'      => 1,
        ]);
    }
}