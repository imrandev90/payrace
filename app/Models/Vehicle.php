<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'registration_id',
        'fleet_number',
        'payment_account',
        'manu_date',
        'owner_id',
        'conductor_id',
        'driver_id',
        'inspector_id',
        'created_by',
    
    ];
    
    
    protected $dates = [
        'manu_date',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/vehicles/'.$this->getKey());
    }
}
