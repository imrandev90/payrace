<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrewStaff extends Model
{
    protected $table = 'crew_staff';

    protected $fillable = [
        'full_name',
        'phone',
        'address',
        'id_number',
        'designation',
        'description',
        'organization_id',
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/crew-staffs/'.$this->getKey());
    }
}
