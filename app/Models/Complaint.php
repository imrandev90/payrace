<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = [
        'phone',
        'title',
        'description',
        'handled_by',
        'handled_at',
    
    ];
    
    
    protected $dates = [
        'handled_at',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/complaints/'.$this->getKey());
    }
}
