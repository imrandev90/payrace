<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComplaintDetail extends Model
{
    protected $fillable = [
        'complaint_id',
        'vehicle_id',
        'route_id',
        'conductor_id',
        'driver_id',
        'organization_id',
        'inspector_id',
        'trip_id',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/complaint-details/'.$this->getKey());
    }
}
