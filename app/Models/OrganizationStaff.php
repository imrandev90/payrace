<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationStaff extends Model
{
    use SoftDeletes;
    protected $table = 'organization_staff';

    protected $fillable = [
        'full_name',
        'email',
        'phone',
        'address',
        'designation',
        'organization_id',
        'password',
        'activated',
        'forbidden',
        'language',
    ];
    
    protected $hidden = [
        'password',
        'remember_token',
    
    ];
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/organization-staffs/'.$this->getKey());
    }
}
