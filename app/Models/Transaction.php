<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'payee_id',
        'phone',
        'full_name',
        'account',
        'paybill',
        'amount',
        'channel',
        'payment_details',
        'time',
        'vehicle_id',
        'route_id',
        'conductor_id',
        'driver_id',
        'organization_id',
        'inspector_id',
        'trip_id',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
        'time',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/transactions/'.$this->getKey());
    }
}
