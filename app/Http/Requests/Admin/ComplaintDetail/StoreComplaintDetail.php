<?php

namespace App\Http\Requests\Admin\ComplaintDetail;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreComplaintDetail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.complaint-detail.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'complaint_id' => ['nullable', 'integer'],
            'vehicle_id' => ['nullable', 'integer'],
            'route_id' => ['nullable', 'integer'],
            'conductor_id' => ['nullable', 'integer'],
            'driver_id' => ['nullable', 'integer'],
            'organization_id' => ['nullable', 'integer'],
            'inspector_id' => ['nullable', 'integer'],
            'trip_id' => ['nullable', 'integer'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
