<?php

namespace App\Http\Requests\Admin\CrewStaff;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCrewStaff extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.crew-staff.edit', $this->crewStaff);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'full_name' => ['sometimes', 'string'],
            'phone' => ['sometimes', 'string'],
            'address' => ['sometimes', 'string'],
            'id_number' => ['sometimes', 'string'],
            'designation' => ['sometimes', 'string'],
            'description' => ['nullable', 'string'],
            'organization_id' => ['nullable', 'integer'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
