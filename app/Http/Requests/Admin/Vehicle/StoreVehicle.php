<?php

namespace App\Http\Requests\Admin\Vehicle;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreVehicle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.vehicle.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'registration_id' => ['required', 'string'],
            'fleet_number' => ['required', 'string'],
            'payment_account' => ['required', 'string'],
            'manu_date' => ['nullable', 'date'],
            'owner_id' => ['nullable', 'integer'],
            'conductor_id' => ['nullable', 'integer'],
            'driver_id' => ['nullable', 'integer'],
            'inspector_id' => ['nullable', 'integer'],
            'created_by' => ['nullable', 'integer'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
