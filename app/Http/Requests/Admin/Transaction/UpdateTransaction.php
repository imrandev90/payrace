<?php

namespace App\Http\Requests\Admin\Transaction;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateTransaction extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.transaction.edit', $this->transaction);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'payee_id' => ['nullable', 'integer'],
            'phone' => ['nullable', 'string'],
            'full_name' => ['sometimes', 'string'],
            'account' => ['nullable', 'string'],
            'paybill' => ['nullable', 'string'],
            'amount' => ['sometimes', 'numeric'],
            'channel' => ['sometimes', 'string'],
            'payment_details' => ['nullable', 'string'],
            'time' => ['nullable', 'date'],
            'vehicle_id' => ['nullable', 'integer'],
            'route_id' => ['nullable', 'integer'],
            'conductor_id' => ['nullable', 'integer'],
            'driver_id' => ['nullable', 'integer'],
            'organization_id' => ['nullable', 'integer'],
            'inspector_id' => ['nullable', 'integer'],
            'trip_id' => ['nullable', 'integer'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
