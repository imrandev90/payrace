<?php

namespace App\Http\Requests\Admin\OrganizationStaff;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateOrganizationStaff extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.organization-staff.edit', $this->organizationStaff);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'full_name' => ['sometimes', 'string'],
            'email' => ['sometimes', 'email', Rule::unique('organization_staff', 'email')->ignore($this->organizationStaff->getKey(), $this->organizationStaff->getKeyName()), 'string'],
            'phone' => ['sometimes', 'string'],
            'address' => ['nullable', 'string'],
            'designation' => ['sometimes', 'string'],
            'organization_id' => ['nullable', 'integer'],
            'password' => ['sometimes', 'confirmed', 'min:7', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/', 'string'],
            'activated' => ['sometimes', 'boolean'],
            'forbidden' => ['sometimes', 'boolean'],
            'language' => ['sometimes', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
