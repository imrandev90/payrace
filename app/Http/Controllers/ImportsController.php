<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\OrganizationsImport;
use App\Imports\OwnersImport;
use App\Imports\RoutesImport;
use App\Imports\DriversImport;
use App\Imports\ConductorsImport;
use App\Imports\VehiclesImport;
use App\Imports\InspectorsImport;
use App\Imports\TransactionsImport;
use App\Imports\TransactionDetailsImport;

use Storage;

class ImportsController extends Controller{
	public function importOrganizations(){
        (new OrganizationsImport)->import('organizations.csv', 'public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

    public function importOwners(){
        (new OwnersImport)->import('owners.csv', 'public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

    public function importRoutes(){
        (new RoutesImport)->import('routes.csv', 'public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

    public function importDrivers(){
        (new DriversImport)->import('drivers.csv', 'public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

    public function importConductors(){
        (new ConductorsImport)->import('conductors.csv', 'public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

    public function importInspectors(){
        (new InspectorsImport)->import('inspectors.csv', 'public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

    public function importTransactions(){
        (new TransactionsImport)->import('transactions.csv', 'public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

    public function importTransactionDetails(){
        (new TransactionDetailsImport)->import('transaction_details.csv','public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

    public function importVehicles(){
        (new VehiclesImport)->import('vehicles.csv','public', \Maatwebsite\Excel\Excel::CSV);
        return 'All good!';
    }

}
