<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OwnersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Owner\BulkDestroyOwner;
use App\Http\Requests\Admin\Owner\DestroyOwner;
use App\Http\Requests\Admin\Owner\IndexOwner;
use App\Http\Requests\Admin\Owner\StoreOwner;
use App\Http\Requests\Admin\Owner\UpdateOwner;
use App\Models\Transaction;
use App\Models\Complaint;

use App\Models\Route;
use App\Models\Owner;
use App\Models\Vehicle;

use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;
use Carbon\Carbon;

class DashboardController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOwner $request
     * @return array|Factory|View
     */
    
    public function index(Request $request){
    	$todayString = "2020-03-08";
    	$today = Carbon::parse($todayString);
    	$weekStartDate = $today->startOfWeek()->format('Y-m-d');

    	// $topEarningRoutes = DB::table('transactions')
    	//    	->whereDate('transactions.time', $todayString)
    	// 	->leftJoin('transaction_details', 'transaction_details.payment_id', 'transactions.id')
    	// 	->leftJoin('routes', 'routes.id', 'transaction_details.route_id')
    	// 	// ->groupBy('transaction_details.route_id')
    	// 	// ->select(['routes.title','routes.number'])
    	// 	->take(20)
     //        ->get();

      //    $topEarningRoutes = DB::table('routes')
    		// ->join('transaction_details', 'transaction_details.route_id', 'routes.id')
    		// ->leftJoin('transactions','transactions.id', 'transaction_details.payment_id')
    		// ->whereDate('transactions.time', $todayString)
    		// // ->leftJoin('transaction_details', 'transaction_details.payment_id', 'transactions.id')
    		// // ->leftJoin('routes', 'routes.id', 'transaction_details.route_id')
    		// // ->whereDate('transactions.time', $todayString)
    		// // ->groupBy('transaction_details.route_id')
    		// // ->select(['routes.title','routes.number'])
    		// // ->take(5000)
      //       ->get();

        
    	// dd($topEarningRoutes);


    	// $todayReport = Transaction::select(\DB::raw("COUNT(*) as count"), \DB::raw("SUM(amount) as sum"))
    	// 					->whereDate('time', $todayString)
    	// 					->first()
    	// 					->only(["count","sum"]);

    	// $weekReport = Transaction::select(\DB::raw("COUNT(*) as count"), \DB::raw("SUM(amount) as sum"))
    	// 					->whereBetween("time", [$weekStartDate." 00:00:00", $todayString." 23:59:59"])
    	// 					->first()
    	// 					->only(["count","sum"]);


    	// $recent = Transaction::latest()->take(10)->get();
    	// $complaintsCount = Complaint::whereDate("created_at",$today->format("Y-m"))->count();
    	// $transactionCount = Transaction::whereDate("created_at",$today->format("Y-m"))->count();



       
        

        $routes = Route::take(10)->pluck("title");

        for ($i=0; $i < 10; $i++) { 
           $data['label'][] = $routes[$i];
           $data['data'][] = ($i+17500) + ($i *50000) ;
        }

     
        $data['chart_data'] = json_encode($data);


        $vehicles = Vehicle::take(5)->get();
        $routes = Route::take(5)->get();
        $transactions = Transaction::take(10)->get();

        $data['vehicles'] = $vehicles;
        $data['transactions'] = $transactions;
        $data['routes'] = $routes;

        // return view('admin.dashboard.index',[
        //     "datat" => $chart_data,
        // 	// "todayReport" => $todayReport,
        // 	// "weekReport" => $weekReport,
        // 	// "recent" => $recent,
        // 	// "complaintsCount" => $complaintsCount,
        // 	// "transactionCount" => $transactionCount,
        // ]);

        return view('admin.dashboard.index',$data);
    }

}
