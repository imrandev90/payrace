<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ComplaintDetailsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ComplaintDetail\BulkDestroyComplaintDetail;
use App\Http\Requests\Admin\ComplaintDetail\DestroyComplaintDetail;
use App\Http\Requests\Admin\ComplaintDetail\IndexComplaintDetail;
use App\Http\Requests\Admin\ComplaintDetail\StoreComplaintDetail;
use App\Http\Requests\Admin\ComplaintDetail\UpdateComplaintDetail;
use App\Models\ComplaintDetail;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class ComplaintDetailsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexComplaintDetail $request
     * @return array|Factory|View
     */
    public function index(IndexComplaintDetail $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ComplaintDetail::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'complaint_id', 'vehicle_id', 'route_id', 'conductor_id', 'driver_id', 'organization_id', 'inspector_id', 'trip_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.complaint-detail.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.complaint-detail.create');

        return view('admin.complaint-detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreComplaintDetail $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreComplaintDetail $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the ComplaintDetail
        $complaintDetail = ComplaintDetail::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/complaint-details'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/complaint-details');
    }

    /**
     * Display the specified resource.
     *
     * @param ComplaintDetail $complaintDetail
     * @throws AuthorizationException
     * @return void
     */
    public function show(ComplaintDetail $complaintDetail)
    {
        $this->authorize('admin.complaint-detail.show', $complaintDetail);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ComplaintDetail $complaintDetail
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ComplaintDetail $complaintDetail)
    {
        $this->authorize('admin.complaint-detail.edit', $complaintDetail);


        return view('admin.complaint-detail.edit', [
            'complaintDetail' => $complaintDetail,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateComplaintDetail $request
     * @param ComplaintDetail $complaintDetail
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateComplaintDetail $request, ComplaintDetail $complaintDetail)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ComplaintDetail
        $complaintDetail->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/complaint-details'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/complaint-details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyComplaintDetail $request
     * @param ComplaintDetail $complaintDetail
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyComplaintDetail $request, ComplaintDetail $complaintDetail)
    {
        $complaintDetail->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyComplaintDetail $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyComplaintDetail $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ComplaintDetail::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(ComplaintDetailsExport::class), 'complaintDetails.xlsx');
    }
}
