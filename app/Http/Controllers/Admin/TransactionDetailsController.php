<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TransactionDetailsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TransactionDetail\BulkDestroyTransactionDetail;
use App\Http\Requests\Admin\TransactionDetail\DestroyTransactionDetail;
use App\Http\Requests\Admin\TransactionDetail\IndexTransactionDetail;
use App\Http\Requests\Admin\TransactionDetail\StoreTransactionDetail;
use App\Http\Requests\Admin\TransactionDetail\UpdateTransactionDetail;
use App\Models\TransactionDetail;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class TransactionDetailsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexTransactionDetail $request
     * @return array|Factory|View
     */
    public function index(IndexTransactionDetail $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(TransactionDetail::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'payment_id', 'vehicle_id', 'route_id', 'conductor_id', 'driver_id', 'organization_id', 'inspector_id', 'trip_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.transaction-detail.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.transaction-detail.create');

        return view('admin.transaction-detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTransactionDetail $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTransactionDetail $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the TransactionDetail
        $transactionDetail = TransactionDetail::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/transaction-details'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/transaction-details');
    }

    /**
     * Display the specified resource.
     *
     * @param TransactionDetail $transactionDetail
     * @throws AuthorizationException
     * @return void
     */
    public function show(TransactionDetail $transactionDetail)
    {
        $this->authorize('admin.transaction-detail.show', $transactionDetail);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TransactionDetail $transactionDetail
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(TransactionDetail $transactionDetail)
    {
        $this->authorize('admin.transaction-detail.edit', $transactionDetail);


        return view('admin.transaction-detail.edit', [
            'transactionDetail' => $transactionDetail,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTransactionDetail $request
     * @param TransactionDetail $transactionDetail
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTransactionDetail $request, TransactionDetail $transactionDetail)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values TransactionDetail
        $transactionDetail->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/transaction-details'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/transaction-details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTransactionDetail $request
     * @param TransactionDetail $transactionDetail
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTransactionDetail $request, TransactionDetail $transactionDetail)
    {
        $transactionDetail->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTransactionDetail $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyTransactionDetail $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    TransactionDetail::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(TransactionDetailsExport::class), 'transactionDetails.xlsx');
    }
}
