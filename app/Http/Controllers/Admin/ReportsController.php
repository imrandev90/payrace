<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OwnersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Owner\BulkDestroyOwner;
use App\Http\Requests\Admin\Owner\DestroyOwner;
use App\Http\Requests\Admin\Owner\IndexOwner;
use App\Http\Requests\Admin\Owner\StoreOwner;
use App\Http\Requests\Admin\Owner\UpdateOwner;
use App\Models\Owner;
use App\Models\Route;
use App\Models\Vehicle;
use App\Models\Transaction;

use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class ReportsController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOwner $request
     * @return array|Factory|View
     */
    public function index(Request $request){

        $vehicles = Vehicle::take(10)->get();
        $owners = Owner::take(10)->get();
        $routes = Route::take(10)->get();
    	
        $hours = [];

        $a = '06:00';
        $b = '21:00';

        $a = strtotime($a);
        $b = strtotime($b);

        $j = 0;

        for($i = 0; $i < $b - $a; $i += 3600) {
            $time = date('H:i', $a + $i);

            $ve['label'][] = $time;
            $ve['data'][] = ($j+1500) + (rand(5000, $j*5000)) ;

            $oe['label'][] = $time;
            $oe['data'][] = ($j+8500) + (rand(20000, $j*20000)) ;

            $re['label'][] = $time;
            $re['data'][] = ($j+17500) + (rand(50000, $j*50000)) ;

            $j++;
        }


        $data = [];
        $data['vehicle_data'] = json_encode($ve);
        $data['owner_data'] = json_encode($oe);
        $data['route_data'] = json_encode($re);

        $data['vehicles'] = $vehicles;
        $data['owners'] = $owners;
        $data['routes'] = $routes;

    	return view('admin.reports.index', $data);
    }

}
