<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ComplaintsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Complaint\BulkDestroyComplaint;
use App\Http\Requests\Admin\Complaint\DestroyComplaint;
use App\Http\Requests\Admin\Complaint\IndexComplaint;
use App\Http\Requests\Admin\Complaint\StoreComplaint;
use App\Http\Requests\Admin\Complaint\UpdateComplaint;
use App\Models\Complaint;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class ComplaintsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexComplaint $request
     * @return array|Factory|View
     */
    public function index(IndexComplaint $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Complaint::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'phone', 'title', 'handled_by', 'handled_at'],

            // set columns to searchIn
            ['id', 'phone', 'title', 'description']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.complaint.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.complaint.create');

        return view('admin.complaint.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreComplaint $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreComplaint $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Complaint
        $complaint = Complaint::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/complaints'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/complaints');
    }

    /**
     * Display the specified resource.
     *
     * @param Complaint $complaint
     * @throws AuthorizationException
     * @return void
     */
    public function show(Complaint $complaint)
    {
        $this->authorize('admin.complaint.show', $complaint);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Complaint $complaint
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Complaint $complaint)
    {
        $this->authorize('admin.complaint.edit', $complaint);


        return view('admin.complaint.edit', [
            'complaint' => $complaint,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateComplaint $request
     * @param Complaint $complaint
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateComplaint $request, Complaint $complaint)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Complaint
        $complaint->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/complaints'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/complaints');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyComplaint $request
     * @param Complaint $complaint
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyComplaint $request, Complaint $complaint)
    {
        $complaint->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyComplaint $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyComplaint $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Complaint::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(ComplaintsExport::class), 'complaints.xlsx');
    }
}
