<?php

namespace App\Http\Controllers\Admin;

use App\Exports\VehiclesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Vehicle\BulkDestroyVehicle;
use App\Http\Requests\Admin\Vehicle\DestroyVehicle;
use App\Http\Requests\Admin\Vehicle\IndexVehicle;
use App\Http\Requests\Admin\Vehicle\StoreVehicle;
use App\Http\Requests\Admin\Vehicle\UpdateVehicle;


use App\Models\Vehicle;
use App\Models\Owner;
use App\Models\CrewStaff;


use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class VehiclesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexVehicle $request
     * @return array|Factory|View
     */
    public function index(IndexVehicle $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Vehicle::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'registration_id', 'fleet_number', 'payment_account', 'manu_date', 'owner_id', 'conductor_id', 'driver_id', 'inspector_id', 'created_by'],

            // set columns to searchIn
            ['id', 'registration_id', 'fleet_number', 'payment_account']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.vehicle.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.vehicle.create');

        $owners = \DB::Table('owners')
                        ->select('id', 'full_name')
                        ->get()
                        ->toJson();

        $drivers = \DB::Table('crew_staff')
                        ->select('id', 'full_name')
                        ->where("designation","=", "Driver")
                        ->get()
                        ->toJson();

        $conductors = \DB::Table('crew_staff')
                        ->select('id', 'full_name')
                        ->where("designation","=", "Conductor")
                        ->get()
                        ->toJson();

        $inspectors = \DB::Table('crew_staff')
                        ->select('id', 'full_name')
                        ->where("designation","=", "Inspector")
                        ->get()
                        ->toJson();    


        return view('admin.vehicle.create',[
            'owners'=>$owners,
            'drivers'=>$drivers,
            'conductors'=>$conductors,
            'inspectors'=>$inspectors,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreVehicle $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreVehicle $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Vehicle
        $vehicle = Vehicle::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/vehicles'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/vehicles');
    }

    /**
     * Display the specified resource.
     *
     * @param Vehicle $vehicle
     * @throws AuthorizationException
     * @return void
     */
    public function show(Vehicle $vehicle)
    {
        $this->authorize('admin.vehicle.show', $vehicle);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Vehicle $vehicle
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Vehicle $vehicle)
    {
        $this->authorize('admin.vehicle.edit', $vehicle);


        $owners = \DB::Table('owners')
                        ->select('id', 'name')
                        ->get()
                        ->toJson();

        $drivers = \DB::Table('crew_staff')
                        ->select('id', 'name')
                        ->where("designation","=", "Driver")
                        ->get()
                        ->toJson();

        $conductors = \DB::Table('crew_staff')
                        ->select('id', 'name')
                        ->where("designation","=", "Conductor")
                        ->get()
                        ->toJson();

        $inspectors = \DB::Table('crew_staff')
                        ->select('id', 'name')
                        ->where("designation","=", "Inspector")
                        ->get()
                        ->toJson();    

        $owner = Owner::find($vehicle->owner_id);
        $driver = CrewStaff::find($vehicle->driver_id);
        $conductor = CrewStaff::find($vehicle->conductor_id);
        $inspector = CrewStaff::find($vehicle->inspector_id);

        return view('admin.vehicle.edit', [
            'vehicle' => $vehicle,

            'owners'=>$owners,
            'drivers'=>$drivers,
            'conductors'=>$conductors,
            'inspectors'=>$inspectors,

            'owner'=>$owner,
            'driver'=>$driver,
            'conductor'=>$conductor,
            'inspector'=>$inspector
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateVehicle $request
     * @param Vehicle $vehicle
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateVehicle $request, Vehicle $vehicle)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Vehicle
        $vehicle->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/vehicles'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/vehicles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyVehicle $request
     * @param Vehicle $vehicle
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyVehicle $request, Vehicle $vehicle)
    {
        $vehicle->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyVehicle $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyVehicle $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Vehicle::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(VehiclesExport::class), 'vehicles.xlsx');
    }
}
