<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrganizationStaffExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OrganizationStaff\BulkDestroyOrganizationStaff;
use App\Http\Requests\Admin\OrganizationStaff\DestroyOrganizationStaff;
use App\Http\Requests\Admin\OrganizationStaff\IndexOrganizationStaff;
use App\Http\Requests\Admin\OrganizationStaff\StoreOrganizationStaff;
use App\Http\Requests\Admin\OrganizationStaff\UpdateOrganizationStaff;
use App\Models\OrganizationStaff;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class OrganizationStaffController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOrganizationStaff $request
     * @return array|Factory|View
     */
    public function index(IndexOrganizationStaff $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(OrganizationStaff::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'full_name', 'email', 'phone', 'address', 'designation', 'organization_id', 'activated', 'forbidden', 'language'],

            // set columns to searchIn
            ['id', 'full_name', 'email', 'phone', 'address', 'designation', 'language']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.organization-staff.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.organization-staff.create');

        $organizations = \DB::Table('organizations')
                        ->select('id', 'title')
                        ->get()
                        ->toJson();


        return view('admin.organization-staff.create',[
            'organizations'=>$organizations,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrganizationStaff $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreOrganizationStaff $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the OrganizationStaff
        $organizationStaff = OrganizationStaff::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/organization-staffs'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/organization-staffs');
    }

    /**
     * Display the specified resource.
     *
     * @param OrganizationStaff $organizationStaff
     * @throws AuthorizationException
     * @return void
     */
    public function show(OrganizationStaff $organizationStaff)
    {
        $this->authorize('admin.organization-staff.show', $organizationStaff);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param OrganizationStaff $organizationStaff
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(OrganizationStaff $organizationStaff)
    {
        $this->authorize('admin.organization-staff.edit', $organizationStaff);

        $organizations = \DB::Table('organizations')
                        ->select('id', 'title')
                        ->get()
                        ->toJson();

        $organization = Organization::find($crewStaff->organization_id);

        return view('admin.organization-staff.edit', [
            'organizationStaff' => $organizationStaff,
            'organizations'=>$organizations,
            'organization'=>$organization,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrganizationStaff $request
     * @param OrganizationStaff $organizationStaff
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateOrganizationStaff $request, OrganizationStaff $organizationStaff)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values OrganizationStaff
        $organizationStaff->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/organization-staffs'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/organization-staffs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyOrganizationStaff $request
     * @param OrganizationStaff $organizationStaff
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyOrganizationStaff $request, OrganizationStaff $organizationStaff)
    {
        $organizationStaff->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyOrganizationStaff $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyOrganizationStaff $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('organizationStaffs')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(OrganizationStaffExport::class), 'organizationStaffs.xlsx');
    }
}
