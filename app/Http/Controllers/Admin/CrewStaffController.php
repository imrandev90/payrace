<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CrewStaffExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CrewStaff\BulkDestroyCrewStaff;
use App\Http\Requests\Admin\CrewStaff\DestroyCrewStaff;
use App\Http\Requests\Admin\CrewStaff\IndexCrewStaff;
use App\Http\Requests\Admin\CrewStaff\StoreCrewStaff;
use App\Http\Requests\Admin\CrewStaff\UpdateCrewStaff;

use App\Models\CrewStaff;
use App\Models\Organization;

use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class CrewStaffController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCrewStaff $request
     * @return array|Factory|View
     */
    public function index(IndexCrewStaff $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(CrewStaff::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'full_name', 'phone', 'address', 'id_number', 'designation', 'organization_id'],

            // set columns to searchIn
            ['id', 'full_name', 'phone', 'address', 'id_number', 'designation', 'description']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.crew-staff.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.crew-staff.create');

        $organizations = \DB::Table('organizations')
                        ->select('id', 'title')
                        ->get()
                        ->toJson();

        return view('admin.crew-staff.create',[
            'organizations'=>$organizations,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCrewStaff $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCrewStaff $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the CrewStaff
        $crewStaff = CrewStaff::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/crew-staffs'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/crew-staffs');
    }

    /**
     * Display the specified resource.
     *
     * @param CrewStaff $crewStaff
     * @throws AuthorizationException
     * @return void
     */
    public function show(CrewStaff $crewStaff)
    {
        $this->authorize('admin.crew-staff.show', $crewStaff);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CrewStaff $crewStaff
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(CrewStaff $crewStaff)
    {
        $this->authorize('admin.crew-staff.edit', $crewStaff);

        $organizations = \DB::Table('organizations')
                        ->select('id', 'title')
                        ->get()
                        ->toJson();

        $organization = Organization::find($crewStaff->organization_id);

        return view('admin.crew-staff.edit', [
            'crewStaff' => $crewStaff,
            'organizations'=> $organizations,
            'organization'=> $organization,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCrewStaff $request
     * @param CrewStaff $crewStaff
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCrewStaff $request, CrewStaff $crewStaff)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values CrewStaff
        $crewStaff->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/crew-staffs'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/crew-staffs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCrewStaff $request
     * @param CrewStaff $crewStaff
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCrewStaff $request, CrewStaff $crewStaff)
    {
        $crewStaff->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCrewStaff $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCrewStaff $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    CrewStaff::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(CrewStaffExport::class), 'crewStaffs.xlsx');
    }
}
