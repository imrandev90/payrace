<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrganizationsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Organization\BulkDestroyOrganization;
use App\Http\Requests\Admin\Organization\DestroyOrganization;
use App\Http\Requests\Admin\Organization\IndexOrganization;
use App\Http\Requests\Admin\Organization\StoreOrganization;
use App\Http\Requests\Admin\Organization\UpdateOrganization;
use App\Models\Organization;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class OrganizationsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOrganization $request
     * @return array|Factory|View
     */
    public function index(IndexOrganization $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Organization::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'title', 'address', 'contact_name', 'contact_phone'],

            // set columns to searchIn
            ['id', 'title', 'address', 'contact_name', 'contact_phone']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.organization.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.organization.create');

        return view('admin.organization.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrganization $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreOrganization $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Organization
        $organization = Organization::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/organizations'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/organizations');
    }

    /**
     * Display the specified resource.
     *
     * @param Organization $organization
     * @throws AuthorizationException
     * @return void
     */
    public function show(Organization $organization)
    {
        $this->authorize('admin.organization.show', $organization);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Organization $organization
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Organization $organization)
    {
        $this->authorize('admin.organization.edit', $organization);


        return view('admin.organization.edit', [
            'organization' => $organization,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrganization $request
     * @param Organization $organization
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateOrganization $request, Organization $organization)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Organization
        $organization->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/organizations'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/organizations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyOrganization $request
     * @param Organization $organization
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyOrganization $request, Organization $organization)
    {
        $organization->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyOrganization $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyOrganization $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Organization::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(OrganizationsExport::class), 'organizations.xlsx');
    }
}
