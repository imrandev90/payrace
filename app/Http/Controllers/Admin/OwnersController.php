<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OwnersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Owner\BulkDestroyOwner;
use App\Http\Requests\Admin\Owner\DestroyOwner;
use App\Http\Requests\Admin\Owner\IndexOwner;
use App\Http\Requests\Admin\Owner\StoreOwner;
use App\Http\Requests\Admin\Owner\UpdateOwner;
use App\Models\Owner;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class OwnersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOwner $request
     * @return array|Factory|View
     */
    public function index(IndexOwner $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Owner::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'full_name', 'phone', 'email', 'address'],

            // set columns to searchIn
            ['id', 'full_name', 'phone', 'email', 'address']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.owner.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.owner.create');

        return view('admin.owner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOwner $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreOwner $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Owner
        $owner = Owner::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/owners'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/owners');
    }

    /**
     * Display the specified resource.
     *
     * @param Owner $owner
     * @throws AuthorizationException
     * @return void
     */
    public function show(Owner $owner)
    {
        $this->authorize('admin.owner.show', $owner);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Owner $owner
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Owner $owner)
    {
        $this->authorize('admin.owner.edit', $owner);


        return view('admin.owner.edit', [
            'owner' => $owner,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOwner $request
     * @param Owner $owner
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateOwner $request, Owner $owner)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Owner
        $owner->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/owners'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/owners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyOwner $request
     * @param Owner $owner
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyOwner $request, Owner $owner)
    {
        $owner->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyOwner $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyOwner $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Owner::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(OwnersExport::class), 'owners.xlsx');
    }
}
