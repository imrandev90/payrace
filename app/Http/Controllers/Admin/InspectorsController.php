<?php

namespace App\Http\Controllers\Admin;

use App\Exports\InspectorsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Inspector\BulkDestroyInspector;
use App\Http\Requests\Admin\Inspector\DestroyInspector;
use App\Http\Requests\Admin\Inspector\IndexInspector;
use App\Http\Requests\Admin\Inspector\StoreInspector;
use App\Http\Requests\Admin\Inspector\UpdateInspector;
use App\Models\Inspector;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\View\View;

class InspectorsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexInspector $request
     * @return array|Factory|View
     */
    public function index(IndexInspector $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Inspector::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'full_name', 'phone', 'address', 'id_number', 'organization_id'],

            // set columns to searchIn
            ['id', 'full_name', 'phone', 'address', 'id_number']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.inspector.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.inspector.create');

        return view('admin.inspector.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreInspector $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreInspector $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Inspector
        $inspector = Inspector::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/inspectors'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/inspectors');
    }

    /**
     * Display the specified resource.
     *
     * @param Inspector $inspector
     * @throws AuthorizationException
     * @return void
     */
    public function show(Inspector $inspector)
    {
        $this->authorize('admin.inspector.show', $inspector);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Inspector $inspector
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Inspector $inspector)
    {
        $this->authorize('admin.inspector.edit', $inspector);


        return view('admin.inspector.edit', [
            'inspector' => $inspector,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateInspector $request
     * @param Inspector $inspector
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateInspector $request, Inspector $inspector)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Inspector
        $inspector->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/inspectors'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/inspectors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyInspector $request
     * @param Inspector $inspector
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyInspector $request, Inspector $inspector)
    {
        $inspector->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyInspector $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyInspector $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Inspector::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Export entities
     *
     * @return BinaryFileResponse|null
     */
    public function export(): ?BinaryFileResponse
    {
        return Excel::download(app(InspectorsExport::class), 'inspectors.xlsx');
    }
}
